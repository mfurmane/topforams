﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoMathematics : MonoBehaviour {

    GameObject fora;
    ChamberBuilder foraminifera;
    public MathPopup popup;
    public GameObject progress;
    float lCenter = 0;
    float lAperture = 0;
    float lCenterP = 0;
    float lApertureP = 0;
    float nCenter = 0;
    float nAperture = 0;

    float triangleSurface(Vector3 a, Vector3 b, Vector3 c)
    {
        b -= a;
        c -= a;
        float x = b.y * c.z - b.z * c.y;
        float y = b.z * c.x - b.x * c.z;
        float z = b.x * c.y - b.y * c.x;

        return 0.5f*Mathf.Sqrt(x*x+y*y+z*z);
    }

    float foraminiferaSurface()
    {
        float surface = 0;
        for (int i = 0; i < foraminifera.chambers.ToArray().Length/2; i++)
        {
            Chambre cha = foraminifera.chambers.ToArray()[i];
            foreach (Triangle t in cha.triangles)
            {
                if (t.gameObject.activeSelf)
                {
                    surface += triangleSurface(t.aa, t.bb, t.cc);
                }
            }
        }
        return surface;
    }
    public static float resolution = 20;

    Vector2 foraminiferaVolume()
    {
        float total = 0;
        float shell = 0;

        Chambre cha0 = foraminifera.chambers.ToArray()[foraminifera.chambers.ToArray().Length / 2 - 1];
        Mesh mesh = new Mesh();

        GameObject f = new GameObject();

        Bounds bounds = foraminifera.foraBounds;

        float init = Time.realtimeSinceStartup;
        float xres = (bounds.size.x) / resolution;
        float yres = (bounds.size.y) / resolution;
        float zres = (bounds.size.z) / resolution;
        float isin = 0;
        float isinin = 0;
        float all = 0;
        for (float i = bounds.min.x; i < bounds.max.x; i += xres)
            for (float j = bounds.min.y; j < bounds.max.y; j += yres)
                for (float k = bounds.min.z; k < bounds.max.z; k += zres)
                {
                    if (cha0.checkInterioranceInRange(new Vector3(i, j, k), 0, foraminifera.chambers.ToArray().Length / 2, true)) isin++;
                    if (cha0.checkInterioranceInRange(new Vector3(i, j, k), foraminifera.chambers.ToArray().Length / 2, foraminifera.chambers.ToArray().Length, true)) isinin++;
                    all++;
                }
        Debug.Log(bounds.size+" - "+(isin/all)+" - "+(isinin/all));
        init = Time.realtimeSinceStartup - init;
        total = (isin / all * bounds.size.x * bounds.size.y * bounds.size.z);
        shell = total - (isinin / all * bounds.size.x * bounds.size.y * bounds.size.z);
        return new Vector2(total, shell);
    }

    string str;

    void distances()
    {
        if (foraminifera.chambers.ToArray().Length / 2 > 1)
        {
            Debug.Log(foraminifera.chambers.ToArray().Length / 2 - 1);
            lCenterP = (foraminifera.chambers.ToArray()[foraminifera.chambers.ToArray().Length / 2 - 1].transform.position - foraminifera.chambers.ToArray()[0].transform.position).magnitude;
            lApertureP = (foraminifera.chambers.ToArray()[foraminifera.chambers.ToArray().Length / 2 - 1].transform.parent.Find("aperture").position - foraminifera.chambers.ToArray()[0].transform.parent.Find("aperture").position).magnitude;
            for (int i = 1; i < foraminifera.chambers.ToArray().Length / 2; i++)
            {
                Chambre cha = foraminifera.chambers.ToArray()[i];
                lCenter += (foraminifera.chambers.ToArray()[i].transform.position - foraminifera.chambers.ToArray()[i - 1].transform.position).magnitude;
                lAperture += (foraminifera.chambers.ToArray()[i].transform.parent.Find("aperture").position - foraminifera.chambers.ToArray()[i - 1].transform.parent.Find("aperture").position).magnitude;
            }
            nCenter = lCenterP / lCenter;
            nAperture = lApertureP / lAperture;
        }
    }

    public void doMath()
    {
        progress.SetActive(true);
        count = true;
        Debug.Log(count);
    }

	// Use this for initialization
	void Start () {
		
	}

    bool count = false;
    // Update is called once per frame
    void FixedUpdate()
    {
        Debug.Log(count);
        if (count)
        {
            Debug.Log("started count");
            distances();
            Vector2 volumes = foraminiferaVolume();
            popup.text.text = "Mathematics done.\nVisible foraminifera surface area: " + foraminiferaSurface() + "\nForaminifera volume: " + volumes.x + "\nShell volume: " + volumes.y
                + "\nSum of distances between chambers centres (Lcenter): " + lCenter + "\nSum of distances between apertures (Laperture): " + lAperture + "\nDistances between first and last chamber centres (Lcenter'): " + lCenterP + "\nDistances between first and last apertures (Laperture'): " + lApertureP
                 + "\nProportion Lcenter'/Lcenter: " + nCenter + "\nProportion Laperture'/Laperture: " + nAperture;
            popup.gameObject.SetActive(true);
            transform.parent.gameObject.SetActive(false);
            progress.SetActive(false);
            count = false;
        }
        if (fora == null)
        {
            if (gameObject.GetComponent<Button>().enabled)
            {
                gameObject.GetComponent<Button>().enabled = false;
                gameObject.GetComponent<Image>().color = Color.black;
            }
            fora = GameObject.Find("foraminifera");
        }
        if (fora != null && !gameObject.GetComponent<Button>().enabled)
        {
            gameObject.GetComponent<Button>().enabled = true;
            gameObject.GetComponent<Image>().color = Color.white;
            foraminifera = fora.GetComponent<ChamberBuilder>();
        }
    }
}
