﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Triangle : MonoBehaviour {

	public Vector3 aa;
	public Vector3 bb;
	public Vector3 cc;

    // Use this for initialization
    public void init(Vector3 a, Vector3 b, Vector3 c)
    {

        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        gameObject.AddComponent<MeshRenderer>();
        mesh.Clear();

        #region Vertices
        Vector3[] vertices = new Vector3[3];

        aa = a;
        bb = b;
        cc = c;

        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;
        #endregion

        #region Normales		
        Vector3[] normales = new Vector3[vertices.Length];
        for (int n = 0; n < vertices.Length; n++)
            normales[n] = vertices[n].normalized;
        #endregion
        #region Triangles
        int[] triangles = new int[vertices.Length * 2];

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 0;
        triangles[4] = 2;
        triangles[5] = 1;

        #endregion

        mesh.vertices = vertices;
        mesh.normals = normales;
        mesh.triangles = triangles;

        mesh.RecalculateBounds();

        gameObject.AddComponent<MeshCollider>();


    }
    // Use this for initialization
    public void init(Vector3 a, Vector3 b, Vector3 c, Vector3 position)
    {

        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        gameObject.AddComponent<MeshRenderer>();
        mesh.Clear();

        #region Vertices
        Vector3[] vertices = new Vector3[3];

        aa = a;
        bb = b;
        cc = c;

        vertices[0] = a;
        vertices[1] = b;
        vertices[2] = c;
        #endregion

        #region Normales		
        Vector3[] normales = new Vector3[vertices.Length];
        for (int n = 0; n < vertices.Length; n++)
            normales[n] = (vertices[n]-position).normalized;
        #endregion
        #region Triangles
        int[] triangles = new int[vertices.Length * 2];

        triangles[0] = 0;
        triangles[1] = 1;
        triangles[2] = 2;
        triangles[3] = 0;
        triangles[4] = 2;
        triangles[5] = 1;

        #endregion

        mesh.vertices = vertices;
        mesh.normals = normales;
        mesh.triangles = triangles;

        mesh.RecalculateBounds();

        gameObject.AddComponent<MeshCollider>();


    }

    void Awake() {
		//init (Vector3.left, Vector3.right, Vector3.up);
	}

	// Update is called once per frame
	void Update () {
		
	}
}
