﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuttingPlane : MonoBehaviour {

    public GameObject cuttingButton;
    bool cutApproved = false;
    bool cutEnded = false;
    ChamberBuilder foraminifera;
    Transform v1;
    Transform v2;
    Transform v3;
    Transform v4;

    public void approveCut()
    {
        cutApproved = true;
    }

    Transform showVertice(Vector3 v)
    {
        GameObject sphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Destroy(sphere1.GetComponent<SphereCollider>());
        sphere1.transform.position = v;
        sphere1.GetComponent<MeshRenderer>().material = transform.Find("Plane").GetComponent<MeshRenderer>().material;
        sphere1.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        sphere1.transform.parent = transform;
        return sphere1.transform;
    }

	// Use this for initialization
	void Awake () {
        v1 = showVertice(new Vector3(-20, transform.position.y, 20));
        v2 = showVertice(new Vector3(20, transform.position.y, 20));
        v3 = showVertice(new Vector3(20, transform.position.y, -20));
        v4 = showVertice(new Vector3(-20, transform.position.y, -20));
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (cutEnded)
        {
            gameObject.SetActive(false);
            cuttingButton.SetActive(false);
            cutEnded = false;
        }
        if (foraminifera != null) if (foraminifera.cutEnded)
        {
            cutEnded = true;
            cutApproved = false;
                foraminifera.cutEnded = false;
        }
        if (cutApproved)
        {
            foraminifera = GameObject.Find("foraminifera").GetComponent<ChamberBuilder>();
            Debug.Log(foraminifera);
            foraminifera.cutEnded = false;
            
            foraminifera.cut(v1, v2, v3, v4, transform);
        }
    }
}
