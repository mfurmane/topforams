﻿using UnityEngine;
using System.Collections;

public class CutPlane : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionStay(Collision collisionInfo) {
		foreach (ContactPoint contact in collisionInfo.contacts) {
			Debug.DrawRay(contact.point, contact.normal * 10, Color.white);
			Debug.Log (contact.point);
		}
	}
	void OnCollisionEnter(Collision collisionInfo) {
		foreach (ContactPoint contact in collisionInfo.contacts) {
			Debug.DrawRay(contact.point, contact.normal * 10, Color.white);
			Debug.Log (contact.point);
		}
	}
	void OnTriggerEnter(Collider other) {
		other.transform.Rotate (new Vector3 (Random.value, Random.value, Random.value));
	}
}
