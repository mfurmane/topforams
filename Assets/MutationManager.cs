﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MutationManager : MonoBehaviour {

    GameObject foramini;
    ChamberBuilder foraminifera;
    public Button bn;
    public Button bp;
    public MutationParameters parameters;
    bool clean = true;
    public int curId = 0;
    public int maxId = 0;
    public List<ChamberBuilder> prevForaminiferas = new List<ChamberBuilder>();
	public GameObject progress;

    float randomize(float value, float range, Vector2 ranges)
    {
        value += UnityEngine.Random.Range(-range, range);
        if (value < ranges.x) value = ranges.x;
        if (value > ranges.y) value = ranges.y;
        return Mathf.Round(value * 100f) / 100f;
    }
    Vector3 randomizeVector(Vector3 value, float range, Vector2 ranges)
    {
        float x = value.x + UnityEngine.Random.Range(-range, range);
        float y = value.y + UnityEngine.Random.Range(-range, range);
        float z = value.z + UnityEngine.Random.Range(-range, range);
        if (x < ranges.x) x = ranges.x;
        if (x > ranges.y) x = ranges.y;
        if (y < ranges.x) y = ranges.x;
        if (y > ranges.y) y = ranges.y;
        if (z < ranges.x) z = ranges.x;
        if (z > ranges.y) z = ranges.y;
        return new Vector3(Mathf.Round(x * 100f) / 100f, Mathf.Round(y * 100f) / 100f, Mathf.Round(z * 100f) / 100f);
    }

    ChamberBuilder generate()
    {
		progress.SetActive (true);
        GameObject newBuilder = new GameObject();
        ChamberBuilder builder = newBuilder.AddComponent<ChamberBuilder>();
        // newBuilder.AddComponent<Rotatable>();
        builder.material = foraminifera.material;
        builder.materials = foraminifera.materials;

        builder.chamberCount = foraminifera.chamberCount2;
        builder.growth_vector_scaling_rate = randomize(foraminifera.growth_vector_scaling_rate, parameters.gvsrValue, Simulate.gvsrRanges);
        builder.chamber_scaling_rates = randomizeVector(foraminifera.chamber_scaling_rates, parameters.scrValue, Simulate.scaRanges);
        builder.first_chamber_size = randomizeVector(foraminifera.first_chamber_size, parameters.firValue, Simulate.firRanges);
        builder.rotation_angle = randomize(foraminifera.rotation_angle, parameters.rotaValue, Simulate.rotaRanges);
        builder.deviation_angle = randomize(foraminifera.deviation_angle, parameters.deviValue, Simulate.deviRanges);
        builder.thickness = randomize(foraminifera.thickness, parameters.thicValue, Simulate.thicRanges);
        builder.simlt = foraminifera.simlt;

        builder.simulate();
        return builder;
    }

    public void nextForaminifera()
    {
        if (maxId == curId)
        {
            //  if (maxId == 0)
            //     prevForaminiferas.Add(foraminifera);
            if (prevForaminiferas[curId] == null) prevForaminiferas[curId] = GameObject.Find("foraminifera").GetComponent<ChamberBuilder>();
            prevForaminiferas[curId].gameObject.SetActive(false);
            foraminifera = generate();
            prevForaminiferas.Add(foraminifera);
            maxId++;
            curId++;
        } else
        {
            prevForaminiferas[curId].gameObject.SetActive(false);
            curId++;
            foraminifera = prevForaminiferas.ToArray()[curId];
            prevForaminiferas[curId].gameObject.SetActive(true);
        }
    }

    public void previousForaminifera()
    {
        prevForaminiferas[curId].gameObject.SetActive(false);
        curId--;
        foraminifera = prevForaminiferas.ToArray()[curId];
        prevForaminiferas[curId].gameObject.SetActive(true);
    }

    public void destroyCurrent()
    {
        Destroy(foraminifera.gameObject);
    }

    public void cleanPrevious()
    {
        if (prevForaminiferas.ToArray().Length > 0)
        {
            for (int i = 0; i < maxId + 1; i++)
            {
                if (i != curId)
                    Destroy(prevForaminiferas[i].gameObject);
                else
                    foraminifera = prevForaminiferas[i];
            }
            prevForaminiferas.Clear();
            prevForaminiferas.Add(foraminifera);
            curId = 0;
            maxId = 0;
            Debug.Log(prevForaminiferas.ToArray().Length);
        }
    }
    public void cleanPreviousPermanently()
    {
        cleanPrevious();
/*        if (prevForaminiferas.ToArray().Length > 0)
        {
            for (int i = 0; i < maxId + 1; i++)
            {
                Destroy(prevForaminiferas[i].gameObject);
            }
            prevForaminiferas.Clear();
            curId = 0;
            maxId = 0;
            Debug.Log(prevForaminiferas.ToArray().Length);
        }*/
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("I am");
    }

    // Update is called once per frame
    void Update()
    {
        

        if (foramini == null)
        {
            bn.enabled = false;
            bn.GetComponent<Image>().color = Color.black;
            bp.enabled = false;
            bp.GetComponent<Image>().color = Color.black;
            foramini = GameObject.Find("foraminifera");
        }
        else
        {
            foraminifera = foramini.GetComponent<ChamberBuilder>();
            if (foraminifera != null)
            {
                if (prevForaminiferas.ToArray().Length == 0)
                {
                    prevForaminiferas.Add(foraminifera);
                }
                if (prevForaminiferas.ToArray().Length > curId)
                {
                    if (!bn.enabled && prevForaminiferas[curId].chamberCount == 0)
                    {
                        bn.enabled = true;
                        bn.GetComponent<Image>().color = Color.white;
                        bp.enabled = true;
                        bp.GetComponent<Image>().color = Color.white;
                    }
                    else if (bn.enabled && prevForaminiferas[curId].chamberCount != 0)
                    {
                        bn.enabled = false;
                        bn.GetComponent<Image>().color = Color.black;
                        bp.enabled = false;
                        bp.GetComponent<Image>().color = Color.black;
                    }
                    if (bn.enabled && !bp.enabled && curId > 0)
                    {
                        bp.enabled = true;
                        bp.GetComponent<Image>().color = Color.white;
                    }
                }
                if (prevForaminiferas.ToArray().Length <= 1 && bp.enabled)
                {
                    bp.enabled = false;
                    bp.GetComponent<Image>().color = Color.black;
                }
                else if (curId == 0)
                {
                    bp.enabled = false;
                    bp.GetComponent<Image>().color = Color.black;
                }
            }
        }
        
    }

    public ChamberBuilder generateNext()
    {
        throw new NotImplementedException();
    }
}
