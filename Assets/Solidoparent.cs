﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Solidoparent : MonoBehaviour {

    public Material main;
    public Material trans;
    public Material current;
    public Sprite transI;
    public Sprite mainI;
    public Image im;
    public Simulate simulate;

    public void converseForaminifera()
    {
        GameObject foraminifera = GameObject.Find("foraminifera");
        if (foraminifera == null) return;
        foraminifera.GetComponent<ChamberBuilder>().material = current;
        foreach (Chambre cha in foraminifera.GetComponent<ChamberBuilder>().chambers)
        {
            cha.material = current;
            foreach (Triangle trl in cha.triangles)
            {
                trl.GetComponent<MeshRenderer>().sharedMaterial = current;
            }
        }
    }

    public void setSolid()
    {
        current = main;
        simulate.material = current;
        im.sprite = transI;
        GameObject foraminifera = GameObject.Find("foraminifera");
        if (foraminifera == null) return;
        foraminifera.GetComponent<ChamberBuilder>().material = current;
        foreach (Chambre cha in foraminifera.GetComponent<ChamberBuilder>().chambers)
        {
            cha.material = current;
            foreach (Triangle trl in cha.triangles)
            {
                trl.GetComponent<MeshRenderer>().sharedMaterial = current;
            }
        }
    }

    public void changeMaterial ()
    {
        if (current.Equals(main))
        {
            im.sprite = mainI;
            current = trans;
        } else
        {
            im.sprite = transI;
            current = main;
        }
        converseForaminifera();
        simulate.material = current;
    }

	// Use this for initialization
	void Start () {
        im = GetComponent<Image>();
        current = main;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
