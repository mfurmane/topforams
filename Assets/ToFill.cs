﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToFill {

    int l = 0;
    List<Vector3> i = new List<Vector3>();
    List<Vector3> o = new List<Vector3>();
    Vector3 position;
    int layer_mask = LayerMask.GetMask("Plane");
    bool tru = false;

    public ToFill() { }

    public void showPoint(Vector3 position)
    {
        GameObject sphere1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        Object.Destroy(sphere1.GetComponent<SphereCollider>());
        sphere1.transform.position = position;
        sphere1.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        sphere1.name = "DEBUG CUT";
    }

    public ToFill (bool abool, bool bbool, bool cbool, Vector3 a, Vector3 b, Vector3 c, Vector3 position)
    {
        this.position = position;
        if (abool)
        {
            l++;
            i.Add(a);
            o.Add(b);
            o.Add(c);
        }
        if (bbool)
        {
            l++;
            i.Add(b);
            o.Add(a);
            o.Add(c);
        }
        if (cbool)
        {
            l++;
            i.Add(c);
            o.Add(b);
            o.Add(a);
        }
    }


    public ToFill(bool abool, bool bbool, bool cbool, Vector3 a, Vector3 b, Vector3 c, Vector3 position, bool tru)
    {
      //  showPoint(a);
     //   showPoint(b);
     //   showPoint(c);

        this.tru = tru;
        this.position = position;
        if (abool)
        {
            l++;
            i.Add(a);
        }
        else o.Add(a);
        if (bbool)
        {
            l++;
            i.Add(b);
        }
        else o.Add(b);
        if (cbool)
        {
            l++;
            i.Add(c);
        }
        else o.Add(c);
    }

    Vector3 getHitPoint(Vector3 p1, Vector3 p2)
    {
        if (tru)
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(p2, p1 - p2), out hit, 999f, layer_mask))
                return hit.point;
            else if (Physics.Raycast(new Ray(p1, p2 - p1), out hit, 999f, layer_mask))
                return hit.point;
            return Vector3.negativeInfinity;
        } else
        {
            RaycastHit hit;
            if (Physics.Raycast(p2, p1 - p2, out hit))
                return hit.point;
            else if (Physics.Raycast(p1, p2 - p1, out hit))
                return hit.point;
            return Vector3.negativeInfinity;
        }
    }

    public List<Triangle> createTriangles()
    {
        List<Triangle> trls = new List<Triangle>();

        if (l == 1)
        {
            GameObject triangle = new GameObject();
            Triangle trl = triangle.AddComponent<Triangle>();

            Vector3 a = i.ToArray()[0];
            Vector3 b = o.ToArray()[0];
            Vector3 c = o.ToArray()[1];
            Vector3 d = getHitPoint(b, a);
            a = getHitPoint(c, a);

            Vector3 ta = a;

            if (!a.Equals(Vector3.negativeInfinity) && !d.Equals(Vector3.negativeInfinity) && !a.Equals(c) && !d.Equals(b) && !a.Equals(d))
            {
                trl.init(b, a, d, position);
                trls.Add(trl);
            }
            else
            {
                if (tru)
                {
                  //  showPoint(a);
                    Debug.Log(ta + " - " + b + " - " + c);
                //    trl.init(ta, b, c, position);
                //    trls.Add(trl);
                } else
                    Object.Destroy(trl.gameObject);
            }
        

            triangle = new GameObject();
            trl = triangle.AddComponent<Triangle>();

            if (!a.Equals(Vector3.negativeInfinity) && !a.Equals(c))
            {
               // Debug.Log("TU BYL BLAD: " + a + " - " + b + " - " + c);
                trl.init(b, c, a, position);
                trls.Add(trl);
            }
            else
            {
                if (tru)
                {
                 //   showPoint(a);
                    Debug.Log(ta + " - " + b + " - " + c);
                //    trl.init(ta, b, c, position);
                 //   trls.Add(trl);
                }
                else
                    Object.Destroy(trl.gameObject);
            }
        }
        else
        {
            GameObject triangle = new GameObject();
            Triangle trl = triangle.AddComponent<Triangle>();
            Vector3 a = i.ToArray()[0];
            Vector3 b = i.ToArray()[1];
            Vector3 c = o.ToArray()[0];
            Vector3 ta = a;
            Vector3 tb = b;
            a = getHitPoint(c, a);
            b = getHitPoint(c, b);
            if (!a.Equals(Vector3.negativeInfinity) && !b.Equals(Vector3.negativeInfinity) && !a.Equals(c) && !b.Equals(c) && !b.Equals(a))
            {
             //   Debug.Log("TU BYL BLAD: " + a + " - " + b + " - " + c);
                trl.init(a, b, c, position);
                trls.Add(trl);
            }
            else
            {
                if (tru)
                {
                  //  showPoint(a);
                 //   showPoint(b);
                    Debug.Log(ta + " - " + tb + " - "+ c);
                //    trl.init(ta, tb, c, position);
                //    trls.Add(trl);
                } else
                Object.Destroy(trl.gameObject);
            }
        }

        return trls;

    }

    public void DrawLine(Vector3 start, Vector3 end)
    {
        GameObject myLine = new GameObject("LINE");
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        //lr.SetColors(color, color);
        //        lr.SetWidth(0.1f, 0.1f);
        lr.startColor = Color.green;
        lr.endColor = Color.yellow;
        lr.startWidth = 0.01f;
        lr.endWidth = 0.01f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }
}

