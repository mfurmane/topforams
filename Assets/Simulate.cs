﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Simulate : MonoBehaviour {

	Button button;
	public Transform chamber;
	public Transform camera;
	public InputField chCo;
	public InputField scRa;
	public InputField scaX;
	public InputField scaY;
	public InputField scaZ;
	public InputField sizX;
	public InputField sizY;
	public InputField sizZ;
	public InputField rota;
    public InputField devi;
    public InputField thic;
    public Smaller smaller;

	public Material material;
	public TextMesh tm;
	public MeshFilter plane;
	public GameObject prefab;

	private ChamberBuilder builder;
	public Material[] materials;
    private bool toDestroy = false;
    private bool canWork = false;

    public static Vector2 gvsrRanges = new Vector2(0.1f, 0.9f);
    public static Vector2 scaRanges = new Vector2(1, 2);
    public static Vector2 firRanges = new Vector2(0.1f, 4f);
    public static Vector2 rotaRanges = new Vector2(-180f, 180f);
    public static Vector2 deviRanges = new Vector2(-90f, 90f);
    public static Vector2 thicRanges = new Vector2(0.01f, 0.9f);
    public MutationManager mutman;

    public Button tools;
    public GameObject progress;

    public void simulate()
    {
        {
            if (int.Parse(chCo.text) > 20)
            {
                smaller.text.text = "Please, be serious. Give me a smaller chamber count.";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(scRa.text) > 0.9f || float.Parse(scRa.text) < 0.1f)
            {
                smaller.text.text = "Really? Growth vector scaling rate have to be between 0.1 and 0.9.";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(scaX.text) > 2f || float.Parse(scaX.text) < 1f || float.Parse(scaY.text) > 2f || float.Parse(scaY.text) < 1f || float.Parse(scaZ.text) > 2f || float.Parse(scaZ.text) < 1f)
            {
                smaller.text.text = "Foraminiferas with chamber scaling rate bigger than 2 look ridicullus, and csr less than 1 can't exist in nature.";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(sizX.text) > 4f || float.Parse(sizX.text) < 0.1f || float.Parse(sizY.text) > 4f || float.Parse(sizY.text) < 0.1f || float.Parse(sizZ.text) > 4f || float.Parse(sizZ.text) < 0.1f)
            {
                smaller.text.text = "Practical purposes forces size less than 4 and bigger than 0.1 (aperture is 0.1).";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(devi.text) > 90 || float.Parse(devi.text) < -90f)
            {
                smaller.text.text = "Spherical coordinate system requires deviation angle between -90 and 90.";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(rota.text) > 180f || float.Parse(rota.text) < -180f)
            {
                smaller.text.text = "Spherical coordinate system requires rotation angle between -180 and 180.";
                smaller.gameObject.SetActive(true);
            }
            else if (float.Parse(thic.text) > 0.9f || float.Parse(thic.text) < 0.01f)
            {
                smaller.text.text = "Thickness is proportion of shell to chamber, has to be between 0.01 and 0.9, because other are senseless.";
                smaller.gameObject.SetActive(true);
            }
            else
            {
                toDestroy = true;
                canWork = true;
            }
        }
    }


	void Awake() {
		button = GetComponent<Button> ();
	
		//chCo = GameObject.Find("ChamberCount").GetComponent<ChamberCount>().d;

	//	button.onClick.AddListener (() => );
		//plane = ane.mesh;

	}
		// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (toDestroy)
        {
            mutman.cleanPreviousPermanently();

            if (builder == null)
            {
                GameObject tmpgo = GameObject.Find("foraminifera");
                if (tmpgo != null) builder = tmpgo.GetComponent<ChamberBuilder>();
            }
            if (builder !=null) Destroy(builder.gameObject);
            toDestroy = false;
            tools.enabled = false;
            tools.GetComponent<Image>().color = Color.black;
        }


        if (canWork && builder == null)
		{
			progress.SetActive(true);
            GameObject newBuilder = new GameObject();
            builder = newBuilder.AddComponent<ChamberBuilder>();
           // newBuilder.AddComponent<Rotatable>();
            builder.material = material;
            builder.materials = materials;

            builder.chamberCount = int.Parse(chCo.text);
            builder.growth_vector_scaling_rate = float.Parse(scRa.text);
            builder.chamber_scaling_rates = new Vector3(float.Parse(scaX.text), float.Parse(scaY.text), float.Parse(scaZ.text));
            builder.first_chamber_size = new Vector3(float.Parse(sizX.text), float.Parse(sizY.text), float.Parse(sizZ.text));
            builder.rotation_angle = float.Parse(rota.text);
            builder.deviation_angle = float.Parse(devi.text);
            builder.thickness = 1-float.Parse(thic.text);
            builder.simlt = this;

            builder.simulate();

            transform.parent.gameObject.GetComponent<SimulationPane>().hidePane();

            canWork = false;
        }

        //Debug.DrawRay (ane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4(ane.mesh.vertices [0]), Vector3.up*100, Color.black);
        //Debug.DrawRay (ane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4(ane.mesh.vertices [10]), Vector3.up*100, Color.black);
        //Debug.DrawRay (ane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4(ane.mesh.vertices [ane.mesh.vertices.Length-6]), Vector3.up*100, Color.black);
        //Debug.Log (chCo.options[chCo.value].text);
    }

    public void enableTools()
    {
        tools.enabled = true;
        tools.GetComponent<Image>().color = Color.white;
    }

    private void simulateForams(int chamberCount, float scalingRate){
		Destroy (GameObject.Find ("Foraminifera"));
		GameObject go = new GameObject ();
		go.name = "Foraminifera";
		Foraminifera foraminiferra = go.AddComponent<Foraminifera> ();
		go.AddComponent<Rotatable>();
		float foramSize = 0;

		for (int x = 0; x < chamberCount; x++) {
			GameObject obj = new GameObject ();
			obj.name = "Chamber";
			obj.transform.parent = go.transform;
			obj.AddComponent<RectTransform> ();
			Chamber cham = obj.AddComponent<Chamber> ();
			MeshRenderer rend = obj.AddComponent<MeshRenderer> ();
			rend.material = material;
			float chamberSize = 3;
			obj.transform.position = new Vector3 (1.5f * chamber.localScale.x * (Mathf.Pow (scalingRate, x) - 1), 1.5f * chamber.localScale.x * (Mathf.Pow (scalingRate, x) - 1) * Random.Range (0.85f, 1.15f), chamber.localScale.x * Random.Range (0.25f, 3.15f));
			for (int i = 0; i < x; i++) {
				chamberSize *= scalingRate;
			}
			foramSize = chamberSize*3;
			obj.transform.localScale = new Vector3(chamberSize*Random.Range(0.5f,1.5f), chamberSize*Random.Range(0.5f,1.5f), chamberSize*Random.Range(0.5f,1.5f));
		

			cham.tmPrefab = tm;
			cham.foram = foraminiferra;
			cham.plane = plane;
			cham.prefab = prefab;
			Debug.Log (tm);
			cham.initiate();
		}

		/*for (int x = 0; x < chamberCount; x++) {
			Transform obj = Instantiate (chamber, new Vector3 (1.5f*chamber.localScale.x*(Mathf.Pow(scalingRate, x)-1), 1.5f*chamber.localScale.x*(Mathf.Pow(scalingRate, x)-1)*Random.Range(0.85f,1.15f), chamber.localScale.x*Random.Range(0.25f,3.15f)), Quaternion.identity, go.transform) as Transform;
			float chamberSize = obj.localScale.x;
			for (int i = 0; i < x; i++) {
				chamberSize *= scalingRate;
			}
			foramSize = chamberSize*3;
			obj.localScale = new Vector3(chamberSize*Random.Range(0.5f,1.5f), chamberSize*Random.Range(0.5f,1.5f), chamberSize*Random.Range(0.5f,1.5f));
		}*/
		camera.position = new Vector3(0,1,-10-foramSize);

	}

}
