﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rounding : MonoBehaviour {

    public float angle = 0;

    private float maxPi = Mathf.PI * 2;
    float R = 4;
    // private Vector3 center = Vector3.zero

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        angle += Time.deltaTime*2;
        if (angle > maxPi) angle -= maxPi;
      //  Debug.Log(angle);
        transform.localPosition = new Vector3(R * Mathf.Cos(angle), R * Mathf.Sin(angle), 0);
    }
}
