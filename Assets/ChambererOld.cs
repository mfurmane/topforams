﻿using UnityEngine;
using System.Collections;

public class Chamber : MonoBehaviour {
	MeshCollider meschCol;
	MeshCollider myCol;
	Mesh mesh;
	private SphereCollider scollider;
	private Vector3 a, b, c;
	public TextMesh tmPrefab;
	public Foraminifera foram;
	public MeshFilter plane;
	Vector3[] vertices;
	ArrayList list;
	Ray[] rays;
	float[] distances;
	int rrr = 0;
	public GameObject prefab;

// a, b, c - na wlasciwej plaszczyznie, ale point jest 0 <= point <= 1
	private bool right(Vector3 point) {
		point = transform.localToWorldMatrix.MultiplyPoint3x4 (point);
		//Debug.Log (c.x * b.y * point.z + c.y * b.z * point.x + c.z * b.x * point.y - c.x * b.z * point.y - c.y * b.x * point.z - c.z * b.y * point.x);
		return c.x * b.y * point.z + c.y * b.z * point.x + c.z * b.x * point.y - c.x * b.z * point.y - c.y * b.x * point.z - c.z * b.y * point.x > 0;
	}

	private Vector3[] twoTriangles(Vector3 x, Vector3 y, Vector3 remz) {
		Vector3[] triangles = new Vector3[2];
		triangles [0] = x;
		triangles [1] = y;
		x = transform.localToWorldMatrix.MultiplyPoint3x4 (x);
		y = transform.localToWorldMatrix.MultiplyPoint3x4 (y);
		remz = transform.localToWorldMatrix.MultiplyPoint3x4 (remz);
		RaycastHit hit;
		Ray ray = new Ray (x, remz-x);
		distances [rrr] = Vector3.Distance (x, remz);
		rays [rrr++] = ray;
		if(Physics.Raycast (ray, out hit, Vector3.Distance(x,remz)*2))
			triangles [0] = transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point);
		ray = new Ray (y, remz-y);
		distances [rrr] = Vector3.Distance (y, remz);
		rays [rrr++] = ray;
		if(Physics.Raycast (ray, out hit, Vector3.Distance(y,remz)*2))
			triangles [1] = transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point);
		//Debug.Log ("#################################################################################### "+triangles[1]+" "+transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point)+" "+" "+transform.localToWorldMatrix.MultiplyPoint3x4(remy)+" "+ transform.localToWorldMatrix.MultiplyPoint3x4(remz));
		return triangles;
	}
	private Vector3[] oneTriangle(Vector3 x, Vector3 remy, Vector3 remz) {
		Vector3[] triangles = new Vector3[2];
		x = transform.localToWorldMatrix.MultiplyPoint3x4 (x);
		remy = transform.localToWorldMatrix.MultiplyPoint3x4 (remy);
		remz = transform.localToWorldMatrix.MultiplyPoint3x4 (remz);
		RaycastHit hit;
		Ray ray = new Ray (x, remy-x);
		distances [rrr] = Vector3.Distance (x, remy);
		rays [rrr++] = ray;
		if(Physics.Raycast (ray, out hit, Vector3.Distance(x,remy)*2))
		triangles [0] = transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point);
		ray = new Ray (x, remz-x);
		distances [rrr] = Vector3.Distance (x, remz);
		rays [rrr++] = ray;
		if(Physics.Raycast (ray, out hit, Vector3.Distance(x,remz)*2))
		triangles [1] = transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point);
		//Debug.Log ("#################################################################################### "+triangles[1]+" "+transform.worldToLocalMatrix.MultiplyPoint3x4(hit.point)+" "+" "+transform.localToWorldMatrix.MultiplyPoint3x4(remy)+" "+ transform.localToWorldMatrix.MultiplyPoint3x4(remz));
		return triangles;
	}

	public void MeshPrepare() {
		mesh.Clear();

		float radius = 1f;
		// Longitude |||
		int nbLong = 9;
		// Latitude ---
		int nbLat = 9;

		float rs = 1f;

		#region Vertices
		list = new ArrayList();
		vertices = new Vector3[(nbLong+1) * nbLat * 2 + 2];
		float _pi = Mathf.PI;
		float _2pi = _pi * 2f;

		//vertices[0] = Vector3.up * radius;
		//if (right(Vector3.up * radius))
			list.Add(Vector3.up * radius);
		for( int lat = 0; lat < nbLat; lat++ )
		{
			float a1 = _pi * (float)(lat+1) / (nbLat+1);
			float sin1 = Mathf.Sin(a1);
			float cos1 = Mathf.Cos(a1);

			for( int lon = 0; lon <= nbLong; lon++ )
			{
				float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
				float sin2 = Mathf.Sin(a2);
				float cos2 = Mathf.Cos(a2);

				//vertices[ lon + lat * (nbLong + 1) + 1] = new Vector3( sin1 * cos2, cos1, sin1 * sin2 ) * radius;

				//if (right(new Vector3( sin1 * cos2, cos1, sin1 * sin2 ) * radius))
					list.Add(new Vector3( sin1 * cos2, cos1, sin1 * sin2 ) * radius);
				//right(vertices[ lon + lat * (nbLong + 1) + 1]);
			}
		}
		//vertices[vertices.Length-1] = Vector3.up * -radius;
		//if (right(Vector3.up * -radius))
			list.Add(Vector3.up * -radius);
		object[] tmp = list.ToArray();
		//vertices = new Vector3[tmp.Length];
		for (int ii = 0; ii < tmp.Length; ii++) {
			vertices[ii]=(Vector3)tmp[ii];
		}
		#endregion

		#region Normales		
		Vector3[] normales = new Vector3[vertices.Length];
		for( int n = 0; n < vertices.Length; n++ )
			normales[n] = vertices[n].normalized;
		#endregion

		#region UVs
		/*Vector2[] uvs = new Vector2[vertices.Length];
		uvs[0] = Vector2.up;
		uvs[uvs.Length-1] = Vector2.zero;
		for( int lat = 0; lat < nbLat; lat++ )
			for( int lon = 0; lon <= nbLong; lon++ )
				uvs[lon + lat * (nbLong + 1) + 1] = new Vector2( (float)lon / nbLong, 1f - (float)(lat+1) / (nbLat+1) );
				*/
		#endregion

		#region Triangles
		int nbFaces = vertices.Length;
		int nbTriangles = nbFaces * 2;
		int nbIndexes = nbTriangles * 3;
		int[] triangles = new int[ nbIndexes * 2 ];

		//Top Cap
		int i = 0;
		/*for( int lon = 0; lon < nbLong; lon++ )
		{
			if (Random.value < rs) {
				triangles[i++] = lon+2;
				triangles[i++] = lon+1;
				triangles[i++] = 0;
			}
		}*/

		int len = list.ToArray().Length;
		//Middle
		for( int lat = 0; lat < nbLat - 1; lat++ )
		{
			for( int lon = 0; lon < nbLong; lon++ )
			{
				int current = lon + lat * (nbLong + 1) + 1;
				int next = current + nbLong + 1;
				//Debug.Log(current+" "+next+" "+vertices[current]+" "+vertices[next]);
				//if (vertices[current]!=Vector3.zero && vertices[current+1]!=Vector3.zero && vertices[next+1]!=Vector3.zero) {
				if (right(vertices[current]) && right(vertices[current + 1]) &&  right(vertices[next + 1])) {
					triangles[i++] = current;
					triangles[i++] = current + 1;
					triangles[i++] = next + 1;
					triangles[i++] = next + 1;
					triangles[i++] = current + 1;
					triangles[i++] = current;
				} else {
					Vector3[] tmppoints = new Vector3[0];
					int thisid = 0;
					if (right(vertices[current]) && !right(vertices[current + 1]) && !right(vertices[next + 1])) {
						Debug.Log("first first");
						tmppoints = oneTriangle(vertices[current], vertices[current + 1], vertices[next + 1]);
						thisid = current; 
					} else if (!right(vertices[current]) && right(vertices[current + 1]) && !right(vertices[next + 1])) {
						Debug.Log("first second");
						tmppoints = oneTriangle(vertices[current + 1], vertices[current], vertices[next + 1]);
						thisid = current + 1;
					} else if (!right(vertices[current]) && !right(vertices[current + 1]) && right(vertices[next + 1])) {
						Debug.Log("first third");
						tmppoints = oneTriangle(vertices[next + 1], vertices[current + 1], vertices[current]);
						thisid = next + 1;
					}
					if (tmppoints.Length!=0) {
						foreach(Vector3 v in tmppoints)
							Debug.Log(v);
						triangles[i++] = thisid;
						vertices[len++] = tmppoints[1];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[0];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[0];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[1];
						triangles[i++] = len-1;
						triangles[i++] = thisid;
					} else {
						int thatid = 0;
						if (right(vertices[current]) && right(vertices[current + 1]) && !right(vertices[next + 1])) {
							thisid = current;
							thatid = current + 1;
							tmppoints = twoTriangles(vertices[current], vertices[current + 1], vertices[next + 1]);
						} else if (!right(vertices[current]) && right(vertices[current + 1]) && right(vertices[next + 1])) {
							thisid = current + 1;
							thatid = next + 1;
							tmppoints = twoTriangles(vertices[current+1], vertices[next + 1], vertices[current]);
						} else if (right(vertices[current]) && !right(vertices[current + 1]) && right(vertices[next + 1])) {
							thisid = next + 1;
							thatid = current;
							tmppoints = twoTriangles(vertices[next + 1], vertices[current], vertices[current + 1]);
						}
						if (tmppoints.Length!=0) {
							Debug.Log("TMPPOINTS 0"+tmppoints[0]);
							Debug.Log("TMPPOINTS 1"+tmppoints[1]);
							triangles[i++] = thisid;
							triangles[i++] = thatid;
							vertices[len++] = tmppoints[1];
							triangles[i++] = len - 1;
							triangles[i++] = len - 1;
							triangles[i++] = thatid;
							triangles[i++] = thisid;
							vertices[len++] = tmppoints[0];
							triangles[i++] = thisid;
							triangles[i++] = len - 1;
							triangles[i++] = len - 2;
							triangles[i++] = len - 2;
							triangles[i++] = len - 1;
							triangles[i++] = thisid;
						}
					}
				}
				//if (vertices[current]!=Vector3.zero && vertices[next+1]!=Vector3.zero && vertices[next]!=Vector3.zero) {
				if (right(vertices[current]) && right(vertices[next + 1]) &&  right(vertices[next])) {
					triangles[i++] = current;
					triangles[i++] = next + 1;
					triangles[i++] = next;
					triangles[i++] = next;
					triangles[i++] = next + 1;
					triangles[i++] = current;
				} else {
					Vector3[] tmppoints = new Vector3[0];
					int thisid=0;
					if (right(vertices[current]) && !right(vertices[next + 1]) && !right(vertices[next])) {
						Debug.Log("second first");
						tmppoints = oneTriangle(vertices[current], vertices[next], vertices[next + 1]);
						thisid = current;
					} else if (!right(vertices[current]) && right(vertices[next + 1]) && !right(vertices[next])) {
						Debug.Log("second second");
						tmppoints = oneTriangle(vertices[next + 1], vertices[next], vertices[current]);
						thisid = next + 1;
					} else if (!right(vertices[current]) && !right(vertices[next + 1]) && right(vertices[next])) {
						Debug.Log("second third");
						tmppoints = oneTriangle(vertices[next], vertices[current], vertices[next + 1]);
						thisid = next;
					}
					if (tmppoints.Length!=0) {
						foreach(Vector3 v in tmppoints)
							Debug.Log(v);
						triangles[i++] = thisid;
						vertices[len++] = tmppoints[1];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[0];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[0];
						triangles[i++] = len-1;
						vertices[len++] = tmppoints[1];
						triangles[i++] = len-1;
						triangles[i++] = thisid;
					} else {
						int thatid = 0;
						if (right(vertices[current]) && right(vertices[next + 1]) && !right(vertices[next])) {
							thisid = current;
							thatid = next + 1;
							tmppoints = twoTriangles(vertices[current], vertices[next + 1], vertices[next]);
						} else if (!right(vertices[current]) && right(vertices[next + 1]) && right(vertices[next])) {
							thisid = next + 1;
							thatid = next;
							tmppoints = twoTriangles(vertices[current+1], vertices[next], vertices[current]);
						} else if (right(vertices[current]) && !right(vertices[next + 1]) && right(vertices[next])) {
							thisid = next;
							thatid = current;
							tmppoints = twoTriangles(vertices[next], vertices[current], vertices[next + 1]);
						}
						if (tmppoints.Length!=0) {
							Debug.Log("TMPPOINTS 0"+tmppoints[0]);
							Debug.Log("TMPPOINTS 1"+tmppoints[1]);
							triangles[i++] = thisid;
							triangles[i++] = thatid;
							vertices[len++] = tmppoints[1];
							triangles[i++] = len - 1;
							triangles[i++] = len - 1;
							triangles[i++] = thatid;
							triangles[i++] = thisid;
							vertices[len++] = tmppoints[0];
							triangles[i++] = thisid;
							triangles[i++] = len - 1;
							triangles[i++] = len - 2;
							triangles[i++] = len - 2;
							triangles[i++] = len - 1;
							triangles[i++] = thisid;
						}
					}
				}
			}
		}

		//Bottom Cap
		for( int lon = 0; lon < nbLong; lon++ )
		{
			if (right(vertices[vertices.Length - 1]) || right(vertices[vertices.Length - (lon+2) - 1]) ||  right(vertices[vertices.Length - (lon+1) - 1])) {
				triangles[i++] = vertices.Length - 1;
				triangles[i++] = vertices.Length - (lon+2) - 1;
				triangles[i++] = vertices.Length - (lon+1) - 1;
			}
		}
		#endregion

		mesh.vertices = vertices;
		mesh.normals = normales;
		//mesh.uv = uvs;
		mesh.triangles = triangles;

		mesh.RecalculateBounds();
		;


		/*foreach( Vector3 pos in vertices )
		{
			TextMesh tm = GameObject.Instantiate( tmPrefab, pos, Quaternion.identity ) as TextMesh;
			tm.name = (i).ToString();
			tm.text = (i++).ToString();
			tm.transform.parent = transform;
		}*/

	}



	// Use this for initialization
	void Awake () {
		a = new Vector3 (-200, 0.0f, 0);
		b = new Vector3 (200, 10.0f, 0);
		c = new Vector3 (0, 0.0f, 346);
        MeshFilter filter = gameObject.AddComponent< MeshFilter >();
		mesh = filter.mesh;
		rays = new Ray[100];
		distances = new float[100];
        
        
	//	scollider = GetComponent<SphereCollider> ();
	//	Mesh mesh;
	//	scollider.
	//	if (scollider
	}

	public void initiate() {
		a = plane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4 (plane.mesh.vertices [0]);
		b = plane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4 (plane.mesh.vertices [10]);
		c = plane.gameObject.transform.localToWorldMatrix.MultiplyPoint3x4 (plane.mesh.vertices [plane.mesh.vertices.Length-6]);
		Instantiate (prefab, a, Quaternion.identity, transform);
		Instantiate (prefab, b, Quaternion.identity, transform);
		Instantiate (prefab, c, Quaternion.identity, transform);
		Debug.Log (tmPrefab);
		MeshPrepare ();
		Debug.Log("Y");
		myCol = gameObject.AddComponent<MeshCollider> ();
		meschCol = GameObject.Find ("Plane").GetComponent<MeshCollider>();
		Debug.Log(myCol.contactOffset);
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		foreach (Vector3 vec in vertices) {
			Vector3 vec2 = transform.localToWorldMatrix.MultiplyPoint3x4 (vec);
			Ray ray = new Ray (vec2, Vector3.down);
			for (int i=0; i<rays.Length;i++){
				//Debug.DrawRay (rays[i].origin, rays[i].direction*distances[i], Color.blue);
			}
			//if (Physics.Raycast (ray, out hit))
			//	Debug.DrawRay (ray.origin, ray.direction * 5, Color.blue);
		}
		//if (Random.value < 0.2f)
			//meschCol.transform.position = new Vector3 (Random.value*5-2.5f, Random.value*5-2.5f, Random.value*5-2.5f);
	}

	void OnCollisionStay(Collision collisionInfo) {
        foreach (ContactPoint contact in collisionInfo.contacts) {
            //Debug.DrawRay(contact.point, contact.normal * 10, Color.white);
			Debug.Log (contact.point);
        }
	}
	void OnTriggerStay(Collider other) {
		Debug.Log (mesh.triangles);
	}
		
}
