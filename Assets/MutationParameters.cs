﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MutationParameters : MonoBehaviour {

    public float gvsrValue = 0.05f;
    public float scrValue = 0.05f;
    public float firValue = 0.05f;
    public float deviValue = 5f;
    public float rotaValue = 5f;
    public float thicValue = 0.05f;

    public InputField gvsrViewer;
    public InputField scrViewer;
    public InputField firViewer;
    public InputField deviViewer;
    public InputField rotaViewer;
    public InputField thicViewer;

    public Slider gvsr;
    public Slider scr;
    public Slider fir;
    public Slider devi;
    public Slider rota;
    public Slider thic;

    public void ok()
    {

    }

    // Use this for initialization
    void Start ()
    {
        gvsr.value = gvsrValue;
        scr.value = scrValue;
        fir.value = firValue;
        devi.value = deviValue;
        rota.value = rotaValue;
        thic.value = thicValue;
        gvsrViewer.text = ""+gvsrValue;
        scrViewer.text = ""+scrValue;
        firViewer.text = ""+firValue;
        deviViewer.text = ""+deviValue;
        rotaViewer.text = ""+rotaValue;
        thicViewer.text = ""+thicValue;

    }

    // Update is called once per frame
    void Update ()
    {
        if (gvsr.value != gvsrValue)
        {
            gvsr.value = Mathf.Round(gvsr.value * 100f) / 100f;
            gvsrValue = gvsr.value;
            gvsrViewer.text = "" + gvsr.value;
        }
        if (float.Parse(gvsrViewer.text) != gvsr.value)
        {
            gvsr.value = float.Parse(gvsrViewer.text);
            gvsrValue = gvsr.value;
        }

        if (devi.value != deviValue)
        {
            devi.value = Mathf.Round(devi.value * 100f) / 100f;
            deviValue = devi.value;
            deviViewer.text = "" + devi.value;
        }
        if (float.Parse(deviViewer.text) != devi.value)
        {
            devi.value = float.Parse(deviViewer.text);
            deviValue = devi.value;
        }

        if (rota.value != rotaValue)
        {
            rota.value = Mathf.Round(rota.value * 100f) / 100f;
            rotaValue = rota.value;
            rotaViewer.text = "" + rota.value;
        }
        if (float.Parse(rotaViewer.text) != rota.value)
        {
            rota.value = float.Parse(rotaViewer.text);
            rotaValue = rota.value;
        }

        if (thic.value != thicValue)
        {
            thic.value = Mathf.Round(thic.value * 100f) / 100f;
            thicValue = thic.value;
            thicViewer.text = "" + thic.value;
        }
        if (float.Parse(thicViewer.text) != thic.value)
        {
            thic.value = float.Parse(thicViewer.text);
            thicValue = thic.value;
        }

        if (scr.value != scrValue)
        {
            scr.value = Mathf.Round(scr.value * 100f) / 100f;
            scrValue = scr.value;
            scrViewer.text = "" + scr.value;
        }
        if (float.Parse(scrViewer.text) != scr.value)
        {
            scr.value = float.Parse(scrViewer.text);
            scrValue = scr.value;
        }

        if (fir.value != firValue)
        {
            fir.value = Mathf.Round(fir.value * 100f) / 100f;
            firValue = fir.value;
            firViewer.text = "" + fir.value;
        }
        if (float.Parse(firViewer.text) != fir.value)
        {
            fir.value = float.Parse(firViewer.text);
            firValue = fir.value;
        }
    }
}
