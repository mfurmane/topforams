﻿using UnityEngine;
using System.Collections;

public class SwitchPlay : MonoBehaviour {

	private AudioSource source;

	// Use this for initialization
	void Awake () {
		source = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void switchPlay() {
		if (source.isPlaying)
			source.Pause ();
		else
			source.Play ();
	}
}
