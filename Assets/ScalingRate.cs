﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScalingRate : MonoBehaviour {

	Dropdown d;

	// Use this for initialization
	void Start () {
		d = (Dropdown)GetComponent("Dropdown");
		List<string> options = new List<string> ();
		for (float i = 0.9f; i > 0f; i-=0.05f) {
			options.Add (""+i);
		}
		d.AddOptions (options);
		/*foreach (Dropdown.OptionData i in d.options) {
			Debug.Log(i.text);
		}*/
	//	GameObject.Find ("Simulate").GetComponent<Simulate> ().scRa = d;
	}

	// Update is called once per frame
	void Update () {

	}
}
