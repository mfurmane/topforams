﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AngleManager : MonoBehaviour {

    public InputField x;
    public InputField y;
    public InputField z;
    public MutationManager mutater;

    private bool notSet = true;
    private GameObject forami;
    private ChamberBuilder foraminifera;
    private Quaternion lastRotation;

    // Use this for initialization
    void Start () {
		
	}
	
    public void setRotation()
    {
        if (foraminifera != null) {
            foraminifera.transform.rotation = Quaternion.Euler(new Vector3(float.Parse(x.text), float.Parse(y.text), float.Parse(z.text)));
        }
    }

	// Update is called once per frame
	void Update () {
		
        if (foraminifera != null && !foraminifera.gameObject.activeSelf)
        {
            foraminifera = mutater.prevForaminiferas[mutater.curId];
        }

        if (foraminifera == null)
        {
            if (notSet)
            {
                x.text = "XXX";
                y.text = "XXX";
                z.text = "XXX";
            }
            forami = GameObject.Find("foraminifera");
            if (forami != null)
            {
                foraminifera = forami.GetComponent<ChamberBuilder>();
                lastRotation = foraminifera.transform.rotation;
                x.text = "" + lastRotation.eulerAngles.x;
                y.text = "" + lastRotation.eulerAngles.y;
                z.text = "" + lastRotation.eulerAngles.z;
            }
        } else
        {
            if (foraminifera.transform.rotation != lastRotation)
            {
                lastRotation = foraminifera.transform.rotation;
                x.text = "" + lastRotation.eulerAngles.x;
                y.text = "" + lastRotation.eulerAngles.y;
                z.text = "" + lastRotation.eulerAngles.z;
            }
        }

	}
}
