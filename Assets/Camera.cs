﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MydCamera : MonoBehaviour {

	public System.Collections.Generic.List<GameObject> objec = new System.Collections.Generic.List<GameObject>();
	private bool move = false;
	private Vector3 opos;
	private Vector3 npos;
	public GameObject buttonPane;
	private Vector2 pos;
	private Vector2 newpos;
	private float ol;
	private float ne;
	private bool scroll = false;
    private Vector3 position;
    private Quaternion rotation;

	// Use this for initialization
	void Start () {
        position = transform.position;
        rotation = transform.rotation;
	}
	
    public void recenterCamera()
    {
        transform.position = position;
        transform.rotation = rotation;
    }

	// Update is called once per frame
	void Update () {
		
		if (Input.touches.Length == 1) {
			Touch touch = Input.touches [0];
			if (touch.tapCount == 3) {
				buttonPane.SetActive (true);
			}
			if (touch.tapCount == 2) {
				if (!move) {
					move = true;
					pos = touch.position;
				} else {
					if (Mathf.Abs (touch.deltaPosition.x) > Mathf.Abs (touch.deltaPosition.y)) {
						if (pos.x > touch.position.x)
							transform.Translate (Vector3.right*0.2f);
						if (pos.x < touch.position.x)
							transform.Translate (Vector3.left*0.2f);
					} else {
						if (pos.y > touch.position.y)
							transform.Translate (Vector3.up*0.2f);
						if (pos.y < touch.position.y)
							transform.Translate (Vector3.down*0.2f);
					}
				}
			}
		} else
			move = false;

		if (Input.touches.Length == 2) {
			Touch touch1 = Input.touches [0];
			Touch touch2 = Input.touches [1];
			if (touch1.tapCount == 2) {
				buttonPane.SetActive (true);
			}
			if (!scroll) {
				pos = touch1.position - touch2.position;
				scroll = true;
			} else {
				newpos = touch1.position - touch2.position;
				ol = pos.x * pos.x + pos.y * pos.y;
				ne = newpos.x * newpos.x + newpos.y * newpos.y;
				if (ol > ne) {
					transform.Translate (Vector3.back);
				} else if (ne > ol) {
					transform.Translate (Vector3.forward);
				}
			}
		} else
			scroll = false;
/*		text.GetComponent<GUIText> ().text = ""+transform.position.x;
		Debug.Log (transform.position.x);
		foreach (Touch touch in Input.touches) {
			Debug.Log (touch.tapCount);
			text.GetComponent<GUIText> ().text = "" + touch.tapCount;
		}
		foreach (Touch touch in Input.touches) {
			if (touch.tapCount == 2) {
				npos = touch.position;
				float max = Mathf.Max (Mathf.Max (Mathf.Abs (npos.x - opos.x), Mathf.Abs (npos.y - opos.y)), Mathf.Abs (npos.z - opos.z));
				float m1 = (npos.x - opos.x);
				float m2 = (npos.y - opos.y);
				//Debug.Log (m1 + " " + m2 + " " + (max + m1) + " " + (max + m2));
				if (touch.deltaPosition.y > 0)
					transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
				else if (touch.deltaPosition.y < 0)
					transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
			}
		}
*/

	}
}
