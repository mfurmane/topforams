﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MutationPopup : MonoBehaviour {

    public Simulate simulate;

    public Text chco;
    public Text gvsr;
    public Text scr;
    public Text fir;
    public Text devi;
    public Text rota;
    public Text thic;

    public Dropdown sizeOfMutationBoard;
    public GameObject screenPrefab;
    public RenderTexture[] renderPrefab;
    public Bounds motherBounds;
    private Material material;
    private Material[] materials;

    private int chamberCount = 5;
    private float gvScalingRate = 0.6f;
    private Vector3 scalingRates = new Vector3(1.05f, 1.05f, 1.05f);
    private Vector3 size = Vector3.one;
    private float deviation = 20;
    private float rotation = 30;
    private float thickness = 0.9f;

    public void setValues(int chamberCount, float gvScalingRate, Vector3 scalingRates, Vector3 size, float deviation, float rotation, float thickness, Bounds motherBounds, Material material, Material[] materials)
    {
        ModeManager.deb.text = "Setting values";
        this.chamberCount = chamberCount;
        this.gvScalingRate = gvScalingRate;
        this.scalingRates = scalingRates;
        this.size = size;
        this.deviation = deviation;
        this.rotation = rotation;
        this.thickness = thickness;
        this.motherBounds = motherBounds;
        this.material = material;
        this.materials = materials;

        ModeManager.deb.text = chco + " - " + chamberCount;

        chco.text = "" + chamberCount;
        gvsr.text = "" + gvScalingRate;
        scr.text = "" + scalingRates;
        fir.text = "" + size;
        devi.text = "" + deviation;
        rota.text = "" + rotation;
        thic.text = "" + thickness;

        Debug.Log(simulate.chCo.text);
        simulate.chCo.text = "" + chamberCount;
        Debug.Log(simulate.chCo.text);
        simulate.scRa.text = "" + gvScalingRate;
        simulate.scaX.text = "" + scalingRates.x;
        simulate.scaY.text = "" + scalingRates.y;
        simulate.scaZ.text = "" + scalingRates.z;
        simulate.sizX.text = "" + size.x;
        simulate.sizY.text = "" + size.y;
        simulate.sizZ.text = "" + size.z;
        simulate.devi.text = "" + deviation;
        simulate.rota.text = "" + rotation;
        simulate.thic.text = "" + thickness;

    }

    ChamberBuilder[] foraminiferas;
    void createMutatedForaminifera(Vector3 position, int id)
    {
        GameObject newBuilder = new GameObject();
        foraminiferas[id] = newBuilder.AddComponent<ChamberBuilder>();
        // newBuilder.AddComponent<Rotatable>();

        foraminiferas[id].material = material;
        foraminiferas[id].materials = materials;

        foraminiferas[id].chamberCount = chamberCount;
        foraminiferas[id].growth_vector_scaling_rate = gvScalingRate ; //
        foraminiferas[id].chamber_scaling_rates = scalingRates ; //
        foraminiferas[id].first_chamber_size = size ; //
        foraminiferas[id].rotation_angle = rotation ; //
        foraminiferas[id].deviation_angle = deviation ; //
        foraminiferas[id].thickness = thickness ; //
        //foraminiferas[id].simlt = this;

        foraminiferas[id].simulate();
        foraminiferas[id].transform.position = position;

        // transform.parent.gameObject.GetComponent<SimulationPane>().hidePane();

        // GameObject o = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        // o.transform.position = position;
    }

    public void mutate()
    {
        int sizeBoard = sizeOfMutationBoard.value + 1;

        motherBounds = GameObject.Find("foraminifera").GetComponent<ChamberBuilder>().foraBounds;
        material = GameObject.Find("foraminifera").GetComponent<ChamberBuilder>().material;
        materials = GameObject.Find("foraminifera").GetComponent<ChamberBuilder>().materials;
        GameObject.Find("foraminifera").SetActive(false);

        motherBounds.size *= sizeBoard;
        Vector3 offset = motherBounds.size / sizeBoard / 2;
        Vector3 minPoint = motherBounds.min;
        minPoint = new Vector3(minPoint.x, minPoint.y, 0);

        foraminiferas = new ChamberBuilder[sizeBoard * sizeBoard];

        for (int i = 0; i < sizeBoard; i++)
        {
            for (int j = 0; j < sizeBoard; j++)
            {
                Debug.Log(minPoint + new Vector3(offset.x * (1 + 2 * i), offset.y * (1 + 2 * j), 0));
                //ChamberBuilder ch = Instantiate(GameObject.Find("foraminifera").GetComponent<ChamberBuilder>(), minPoint + new Vector3(offset.x * (1 + 2 * i), offset.y * (1 + 2 * j), 0), transform.rotation);
                createMutatedForaminifera(new Vector3(i*4, j*4, 0), i * sizeBoard + j);
                foraminiferas[i * sizeBoard + j].gameObject.SetActive(false);
            }
        }
        Camera cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        Debug.Log(cam.WorldToViewportPoint(minPoint));


   //     GameObject.Find("Main Camera").transform.position = new Vector3(maxmax.x, maxmax.y, -10);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.activeSelf) if (Input.GetMouseButtonDown(0)) gameObject.SetActive(false);
    }
}



/*
  for (int i = 0; i < sizeBoard; i++)
        {
            for (int j = 0; j < sizeBoard; j++)
            {
                GameObject screen = Instantiate(screenPrefab);
                Debug.Log(screen.transform.Find("Camera").GetComponent<Camera>().targetTexture);
                RenderTexture rt = Instantiate(renderPrefab[i]);
                rt.Create();
                screen.transform.Find("Camera").GetComponent<Camera>().targetTexture = rt;
                screen.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = rt;
                //                RenderTexture rt = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
                //              rt.Create();
                //            rt.
                Vector3 leftUpCorner = GameObject.Find("Main Camera").transform.position - screen.transform.localScale / 2;
                leftUpCorner = new Vector3(leftUpCorner.x, leftUpCorner.y, 0);
                screen.transform.parent = GameObject.Find("Main Camera").transform;
                screen.transform.localScale /= sizeBoard;
                Vector3 offset = screen.transform.localScale / 2;
                screen.transform.position = leftUpCorner + new Vector3(offset.x * (1 + 2*i), offset.y * (1 + 2*j), 0);
                Debug.Log("MUTATE: " + screen.transform.localScale + " - " + screen.transform.localScale);
                createMutatedForaminifera(new Vector3(screen.transform.position.x, screen.transform.position.y, 10));
            }
        }
*/