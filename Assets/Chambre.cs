﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chambre : MonoBehaviour {

	public bool DEBUG_MODE = false;

	public Material[] materials;

	private ChamberBuilder builder;
	public Material material;
	public Vector3[] vertices;
    public List<Triangle> triangles = new List<Triangle>();
    public List<ShadowTriangle> shadowTriangles = new List<ShadowTriangle>();
    public List<Chambre> chambers;
	public Vector3 apert;
    public Vector3 previousAperture;
	public Vector3 radius;
    private List<Triangle> toRemove = new List<Triangle>();
    private List<Triangle> toAdd = new List<Triangle>();
    private List<ToFill> toFill = new List<ToFill>();
    private bool fillHoles = false;
    public bool cleaningFinished = false;


    private Vector3 min;

//	private Vector3 pos;
	int qpa = 0;

	public Vector3 showPoint(Vector3 position, Material material, string name) {
		GameObject sphere1 = GameObject.CreatePrimitive (PrimitiveType.Sphere);
        Destroy(sphere1.GetComponent<SphereCollider>());
		sphere1.transform.position = position;
		sphere1.GetComponent<MeshRenderer> ().material = material;
		sphere1.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		sphere1.name = name;
		sphere1.transform.parent = transform.parent;
		return position;
	}

    float distanceFromAperture(Vector3 point)
    {
        return Vector3.Distance(point, previousAperture);
    }


    void createTriangle(Vector3 uno, Vector3 duo, Vector3 tre)
    {
        GameObject triangle = new GameObject();
        Triangle trl = triangle.AddComponent<Triangle>();
        trl.transform.SetParent(transform);
        trl.init(uno, duo, tre);
        trl.gameObject.GetComponent<MeshRenderer>().material = material;
        triangles.Add(trl);
    }

    void createShadowTriangle(Vector3 uno, Vector3 duo, Vector3 tre)
    {
        GameObject triangle = new GameObject("SHADOW TRIANGLE");
        ShadowTriangle trl = triangle.AddComponent<ShadowTriangle>();
        trl.transform.SetParent(transform);
        trl.init(uno, duo, tre);
        shadowTriangles.Add(trl);
        /*showPoint(uno, materials[2], "");
        showPoint(duo, materials[2], "");
        showPoint(tre, materials[2], "");*/
    }

    //TOREMOVE
    void createTriangle(Vector3 uno, Vector3 duo, Vector3 tre, Material mat) {
		GameObject triangle = new GameObject();
		Triangle trl = triangle.AddComponent<Triangle> ();
		trl.transform.SetParent (transform);
		trl.init (uno, duo, tre);
		if (DEBUG_MODE)
			trl.gameObject.GetComponent<MeshRenderer> ().material = mat;
		else
			trl.gameObject.GetComponent<MeshRenderer> ().material = material;
		triangles.Add (trl);
	}

	Vector3 getHitPoint(Vector3 uno, Vector3 duo) {
		RaycastHit hit;
		if (Physics.Raycast (duo, uno - duo, out hit))
			return hit.point;
		return Vector3.zero;
	}

    //TOREMOVE
	void fillTriangles(int unoi, int duoi, int trei) {
		//Debug.Log (materials);
		Vector3 uno = vertices [unoi];
		Vector3 duo = vertices [duoi];
		Vector3 tre = vertices [trei];
		string qpa = uno + " " + duo + " " + tre;
		Vector3 tmp;
		switch (Functions.whoIsOutside (uno, duo, tre, builder.chambers)) {
		case Functions.Outside.UNO:

			duo = getHitPoint (uno, duo);
			tre = getHitPoint (uno, tre);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[0]);
			break;
		case Functions.Outside.DUO:
			uno = getHitPoint (duo, uno);
			tre = getHitPoint (duo, tre);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[1]);
			break;
		case Functions.Outside.TRE:
			uno = getHitPoint (tre, uno);
			duo = getHitPoint (tre, duo);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[2]);
			break;
		case Functions.Outside.UNODUO:
			tmp = getHitPoint (uno, tre);
			tre = getHitPoint (duo, tre);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[3]);
			if (uno != tmp && uno != tre && tmp != tre) createTriangle (uno, tre, tmp, materials[3]);
			break;
		case Functions.Outside.UNOTRE:
			tmp = getHitPoint (tre, duo);
			duo = getHitPoint (uno, duo);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[4]);
			if (duo != tmp && duo != tre && tmp != tre) createTriangle (duo, tmp, tre, materials[4]);
			break;
		case Functions.Outside.DUOTRE:
			tmp = getHitPoint (tre, uno);
			uno = getHitPoint (uno, duo);
			if (uno != duo && uno != tre && duo != tre) createTriangle (uno, duo, tre, materials[5]);
			if (uno != tmp && uno != tre && tmp != tre) createTriangle (uno, tre, tmp, materials[5]);
			break;
		}
	}

    void addTriangle(int uno, int duo, int tre)
    {
        //Functions.Wheris wheris = Functions.pointPosition(vertices[uno], vertices[duo], vertices[tre], builder.chambers);
        createTriangle(vertices[uno], vertices[duo], vertices[tre]);
        /*		if (builder.chambers.Count == 0 || wheris == Functions.Wheris.OUT) {
                    createTriangle (vertices [uno], vertices [duo], vertices [tre]);
                } else if (wheris == Functions.Wheris.BORDER) {
                    fillTriangles (uno, duo, tre);
                } */
    }
    void addShadowTriangle(int uno, int duo, int tre)
    {
        createShadowTriangle(vertices[uno], vertices[duo], vertices[tre]);
    }

    public static int modulo = 2;
    // Longitude |||
    public static int nbLong = 12;
    // Latitude ---
    public static int nbLat = 6;

    public void buildChamber(Vector3 radius, Vector3 position, int id) {
        if (chambers.ToArray().Length > 0) {
            Chambre prevCha = chambers.ToArray()[chambers.ToArray().Length - 1];
            GameObject prvap = prevCha.transform.parent.Find("aperture").gameObject;
            previousAperture = prvap.transform.position;
        }
        this.radius = radius;
        gameObject.name = "chamber-" + id;
        builder = transform.parent.parent.GetComponent<ChamberBuilder>();
        material = builder.material;
        materials = builder.materials;
        MeshFilter filter = gameObject.AddComponent<MeshFilter>();
        Mesh mesh = filter.mesh;
        mesh.Clear();

        vertices = new Vector3[(nbLong + 1) * nbLat + 2];
        float _pi = Mathf.PI;
        float _2pi = _pi * 2f;

        //	vertices[0] = Vector3.up * radius.y + position;
        vertices[0] = Vector3.up * radius.y;
        for (int lat = 0; lat < nbLat; lat++)
        {
            float a1 = _pi * (float)(lat + 1) / (nbLat + 1);
            float sin1 = Mathf.Sin(a1);
            float cos1 = Mathf.Cos(a1);

            for (int lon = 0; lon <= nbLong; lon++)
            {
                float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
                float sin2 = Mathf.Sin(a2);
                float cos2 = Mathf.Cos(a2);

                //	vertices[ lon + lat * (nbLong + 1) + 1] = new Vector3( sin1 * cos2 * radius.x, cos1 * radius.y, sin1 * sin2 * radius.z) + position;
                vertices[lon + lat * (nbLong + 1) + 1] = new Vector3(sin1 * cos2 * radius.x, cos1 * radius.y, sin1 * sin2 * radius.z);
            }
        }
        //	vertices[vertices.Length-1] = Vector3.up * -radius.y + position;
        vertices[vertices.Length - 1] = Vector3.up * -radius.y;

        Vector3[] normales = new Vector3[vertices.Length];
        for (int n = 0; n < vertices.Length; n++)
            normales[n] = (vertices[n]).normalized;
        //	normales[n] = (vertices[n]-position).normalized;

        #region UVs
        Vector2[] uvs = new Vector2[vertices.Length];
        uvs[0] = Vector2.up;
        uvs[uvs.Length - 1] = Vector2.zero;
        for (int lat = 0; lat < nbLat; lat++)
            for (int lon = 0; lon <= nbLong; lon++)
                uvs[lon + lat * (nbLong + 1) + 1] = new Vector2((float)lon / nbLong, 1f - (float)(lat + 1) / (nbLat + 1));
        #endregion

        #region Triangles

        //Top Cap
        for (int lon = 0; lon < nbLong; lon++)
        {
            addTriangle(lon + 2, lon + 1, 0);
            if (lon % modulo ==0) addShadowTriangle(lon + 2, lon + 1, 0);
        }
        //addShadowTriangle(2, 1, 0);
        //addShadowTriangle(nbLong / 2 + 2, nbLong / 2 + 1, nbLong / 2);

        //Middle
        for (int lat = 0; lat < nbLat - 1; lat++)
        {
            for (int lon = 0; lon < nbLong; lon++)
            {
                int current = lon + lat * (nbLong + 1) + 1;
                int next = current + nbLong + 1;

                addTriangle(current, current + 1, next + 1);
                addTriangle(current, next + 1, next);
               // if (lon % (modulo / 2) == 0)
                addShadowTriangle(current, current + 1, next + 1);
                    //addShadowTriangle(current, next + 1, next);
               // }

            }
        }

        //Bottom Cap
        for (int lon = 0; lon < nbLong; lon++)
        {
            addTriangle(vertices.Length - 1, vertices.Length - (lon + 2) - 1, vertices.Length - (lon + 1) - 1);
            if (lon % modulo == 0) addShadowTriangle(vertices.Length - 1, vertices.Length - (lon + 2) - 1, vertices.Length - (lon + 1) - 1);
        }
        //addShadowTriangle(vertices.Length - 1, vertices.Length - 3, vertices.Length - 2);
        //addShadowTriangle(vertices.Length - 1, vertices.Length - 3 - nbLong / 2, vertices.Length - 2 - nbLong / 2);
        #endregion
      //  Debug.Log("TRIANGLES IN CHAMBER "+id+": "+triangles.ToArray().Length+" AND SHADOW TRIANGLES: "+shadowTriangles.ToArray().Length);
        transform.parent.position = position;

    }

    public bool checkInteriorance(Vector3 point)
    {
        return checkInterioranceInRange(point, 0, chambers.ToArray().Length, false);
    }

    public bool checkInterioranceInRange(Vector3 point, int first, int last, bool dontCare)
    {
        if (chambers.ToArray().Length == 0) return false;

        bool tmp = true;

        for (int i = first; i < last; i++)
        {
            Chambre otherChamber = chambers.ToArray()[i];
            if (dontCare || (!dontCare && otherChamber.name != name))
            {
                foreach (ShadowTriangle triang in otherChamber.shadowTriangles)
                {
                    Vector3 one = triang.transform.TransformPoint(triang.bb) - triang.transform.TransformPoint(triang.aa);
                    Vector3 two = triang.transform.TransformPoint(triang.cc) - triang.transform.TransformPoint(triang.aa);
                    Vector3 three = point - triang.transform.TransformPoint(triang.aa);
                    float det = one.x * two.y * three.z + two.x * three.y * one.z + three.x * one.y * two.z - one.z * two.y * three.x - one.y * two.x * three.z - one.x * two.z * three.y;
                    if (det > 0)
                    {
                        tmp = false;
                        break;
                    }
                }
                if (tmp) return true;
                else tmp = true;
            }
        }
        return false;
    }


    public void findAperture()
    {
        Chambre[] chams = chambers.ToArray();
        Vector3 aperture = Vector3.zero;
        if (chams.Length > 0)
        {
            showPoint(findEveryNextAperture(), materials[5], "aperture");
        } else
        {
            RaycastHit hit;
            if (radius.x < radius.y)
                if (radius.x < radius.z)
                {
                    if (Physics.Raycast(transform.position, transform.right, out hit))
                    {
                        if (hit.point != null)
                        {
                            aperture = hit.point;
                        }
                    }
                }
                else
                {
                    if (Physics.Raycast(transform.position, transform.forward, out hit))
                    {
                        if (hit.point != null)
                        {
                            aperture = hit.point;
                        }
                    }
                }
            else if (radius.y < radius.z)
            {
                if (Physics.Raycast(transform.position, transform.up, out hit))
                {
                    if (hit.point != null)
                    {
                        aperture = hit.point;
                    }
                }
            }
            else
                if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                if (hit.point != null)
                {
                    aperture = hit.point;
                }
            }
            showPoint(aperture, materials[5], "aperture");


        }
    }

    Vector3 findEveryNextAperture()
    {
        RaycastHit hit;
        min = new Vector3(999999, 999999, 999999);
        previousAperture += 0.01f * (previousAperture - chambers.ToArray()[chambers.ToArray().Length-1].transform.position);

        int nbLong = 124;
        int nbLat = 93;
        float _pi = Mathf.PI;
        float _2pi = _pi * 3f;
     //   Debug.Log("PREVIOUS APERTURE AT SEARCHING: "+previousAperture);
        if (Physics.Raycast(previousAperture, Vector3.up, out hit))
        {
            //DrawLine(previousAperture, previousAperture + Vector3.up);
            if (hit.point != null && hit.collider.transform.parent.gameObject == gameObject)
            {
                if ((min - previousAperture).magnitude > (hit.point - previousAperture).magnitude)
                {
                  //  DrawLine(previousAperture, hit.point);
                 //   Debug.Log("UP");
                    min = hit.point;
                }
            }
        }
        for (int lat = 0; lat < nbLat; lat++)
        {
            float a1 = _pi * (float)(lat + 1) / (nbLat + 1);
            float sin1 = Mathf.Sin(a1);
            float cos1 = Mathf.Cos(a1);

            for (int lon = 0; lon <= nbLong; lon++)
            {
                float a2 = _2pi * (float)(lon == nbLong ? 0 : lon) / nbLong;
                float sin2 = Mathf.Sin(a2);
                float cos2 = Mathf.Cos(a2);
                if (Physics.Raycast(previousAperture, new Vector3(sin1 * cos2 * radius.x, cos1 * radius.y, sin1 * sin2 * radius.z), out hit, 9999f))
                {
                   // if (lon%5==0&&lat%5==0)
                   //     DrawLine(previousAperture, previousAperture + new Vector3(sin1 * cos2 * radius.x, cos1 * radius.y, sin1 * sin2 * radius.z));
                    // DrawLine(previousAperture, previousAperture + new Vector3(sin1 * cos2 * radius.x, cos1 * radius.y, sin1 * sin2 * radius.z));
                    if (hit.point != null && hit.collider.transform.parent.gameObject == gameObject)
                    {
                     //   if (lon % 5 == 0 && lat % 5 == 0)
                       //     DrawLine(previousAperture, hit.point);

                        if ((min - previousAperture).magnitude > (hit.point - previousAperture).magnitude)
                        {
                            min = hit.point;
                        }
                    }
                }
            }
        }
        if (Physics.Raycast(previousAperture, Vector3.down, out hit))
        {
            if (hit.point != null && hit.collider.transform.parent.gameObject == gameObject)
            {
              //  DrawLine(previousAperture, hit.point);
                if ((min - previousAperture).magnitude > (hit.point - previousAperture).magnitude)
                {
                  //  DrawLine(previousAperture, hit.point);
                    Debug.Log("DOWN");
                    min = hit.point;
                }
            }
        }
        return min;
    }



    public void cleanInterior()
    {
        bool a = false;
        bool b = false;
        bool c = false;
        foreach (Triangle triang in triangles)
        {
            //CHECKING INTERIORANCE
            int inside = 0;
            if (checkInteriorance(triang.transform.TransformPoint(triang.aa)))
            {
                a = true;
                inside++;
            }
            if (checkInteriorance(triang.transform.TransformPoint(triang.bb)))
            {
                b = true;
                inside++;
            }
            if (checkInteriorance(triang.transform.TransformPoint(triang.cc)))
            {
                c = true;
                inside++;
            }

            //DEALING WITH INTERIORANCE
            if (inside == 1 || inside == 2)
            {
                addToFill(a, b, c, triang.transform.TransformPoint(triang.aa), triang.transform.TransformPoint(triang.bb), triang.transform.TransformPoint(triang.cc));
            }
            if (inside > 0)
            {
                toRemove.Add(triang);
                Destroy(triang.gameObject);
            }

            a = false;
            b = false;
            c = false;
        }
        foreach (Triangle triang in toRemove)
        {
            triangles.Remove(triang);
        }
        fillHoles = true;
    }

    private void addToFill(bool a, bool b, bool c, Vector3 vector31, Vector3 vector32, Vector3 vector33)
    {
        toFill.Add(new ToFill(a, b, c, vector31, vector32, vector33, transform.position));
    }

    private void addToFill(bool a, bool b, bool c, Triangle triang)
    {
        /*
        ToFill tf = new GameObject().AddComponent<ToFill>();
        tf.transform.parent = transform;
        if (a) tf.inside.Add(vector31);
        else tf.outside.Add(vector31);
        if (b) tf.inside.Add(vector32);
        else tf.outside.Add(vector32);
        if (c) tf.inside.Add(vector33);
        else tf.outside.Add(vector33);
        toFill.Add(tf);
        */
    }

    private void findMissingTriangles()
    {
        GameObject go = new GameObject("filling triangles keeper");
        go.transform.parent = transform;
        foreach (ToFill tf in toFill)
        {
            foreach (Triangle trl in tf.createTriangles())
            {
                trl.transform.SetParent(go.transform);
                trl.gameObject.GetComponent<MeshRenderer>().material = material;
                triangles.Add(trl);
            }
        }
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (fillHoles)
        {
            findMissingTriangles();
            fillHoles = false;
            cleaningFinished = true;
        }
	}




    public void DrawLine(Vector3 start, Vector3 end)
    {
        GameObject myLine = new GameObject("LINE");
        myLine.transform.position = start;
        myLine.transform.parent = transform.parent;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        //lr.SetColors(color, color);
        //        lr.SetWidth(0.1f, 0.1f);
        lr.startColor = Color.green;
        lr.endColor = Color.yellow;
        lr.startWidth = 0.01f;
        lr.endWidth = 0.01f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

}
