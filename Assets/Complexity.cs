﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Complexity : MonoBehaviour {

    public InputField lonViewer;
    public InputField latViewer;
    public InputField modViewer;
    public InputField monViewer;

    public Slider lon;
    public Slider lat;
    public Slider mod;
    public Slider mon;

    // Use this for initialization
    void Start () {
        lon.value = Chambre.nbLong;
        lat.value = Chambre.nbLat;
        mod.value = Chambre.modulo;
        mon.value = DoMathematics.resolution;
        lonViewer.text = "" + Chambre.nbLong;
        latViewer.text = "" + Chambre.nbLat;
        modViewer.text = "" + Chambre.modulo;
        monViewer.text = "" + DoMathematics.resolution;
    }
	
	// Update is called once per frame
	void Update () {
        if (lon.value != Chambre.nbLong)
        {
            lon.value = Mathf.Round(lon.value * 100f) / 100f;
            Chambre.nbLong = (int)lon.value;
            lonViewer.text = "" + lon.value;
        }
        if (float.Parse(lonViewer.text) != lon.value)
        {
            lon.value = float.Parse(lonViewer.text);
            Chambre.nbLong = (int)lon.value;
        }

        if (lat.value != Chambre.nbLat)
        {
            lat.value = Mathf.Round(lat.value * 100f) / 100f;
            Chambre.nbLat = (int)lat.value;
            latViewer.text = "" + lat.value;
        }
        if (float.Parse(latViewer.text) != lat.value)
        {
            lat.value = float.Parse(latViewer.text);
            Chambre.nbLat = (int)lat.value;
        }

        if (mod.value != Chambre.modulo)
        {
            mod.value = Mathf.Round(mod.value * 100f) / 100f;
            Chambre.modulo = (int)mod.value;
            modViewer.text = "" + mod.value;
        }
        if (float.Parse(modViewer.text) != mod.value)
        {
            mod.value = float.Parse(modViewer.text);
            Chambre.modulo = (int)mod.value;
        }

        if (mon.value != DoMathematics.resolution)
        {
            mon.value = Mathf.Round(mon.value * 100f) / 100f;
            DoMathematics.resolution = mon.value;
            monViewer.text = "" + mon.value;
        }
        if (float.Parse(monViewer.text) != mon.value)
        {
            mon.value = float.Parse(monViewer.text);
            DoMathematics.resolution = mon.value;
        }
        
    }
}
