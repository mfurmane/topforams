﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SimulationPane : MonoBehaviour {

	public ShowSimulationPane shower;
    public GameObject paneButton;

	int chCoValue;
	float scRaValue;
	float scaXValue;
	float scaYValue;
	float scaZValue;
	float sizXValue;
	float sizYValue;
	float sizZValue;
	float rotaValue;
	float deviValue;
	float thicValue;
	int chCoValueDefault;
	float scRaValueDefault;
	float scaXValueDefault;
	float scaYValueDefault;
	float scaZValueDefault;
	float sizXValueDefault;
	float sizYValueDefault;
	float sizZValueDefault;
	float rotaValueDefault;
	float deviValueDefault;
	float thicValueDefault;

    public InputField chCo;
    public InputField scRa;
    public InputField scaX;
    public InputField scaY;
    public InputField scaZ;
    public InputField sizX;
    public InputField sizY;
    public InputField sizZ;
    public InputField rota;
    public InputField devi;
    public InputField thic;

    public Slider chCoSlider;
    public Slider scRaSlider;
    public Slider scaXSlider;
    public Slider scaYSlider;
    public Slider scaZSlider;
    public Slider sizXSlider;
    public Slider sizYSlider;
    public Slider sizZSlider;
    public Slider rotaSlider;
    public Slider deviSlider;
    public Slider thicSlider;

    // Use this for initialization
    void Start ()
	{
		chCoValue = int.Parse(chCo.text);
		scRaValue = float.Parse(scRa.text);
		scaXValue = float.Parse(scaX.text);
		scaYValue = float.Parse(scaY.text);
		scaZValue = float.Parse(scaZ.text);
		sizXValue = float.Parse(sizX.text);
		sizYValue = float.Parse(sizY.text);
		sizZValue = float.Parse(sizZ.text);
		rotaValue = float.Parse(rota.text);
		deviValue = float.Parse(devi.text);
		thicValue = float.Parse(thic.text);
		chCoValueDefault = int.Parse(chCo.text);
		scRaValueDefault = float.Parse(scRa.text);
		scaXValueDefault = float.Parse(scaX.text);
		scaYValueDefault = float.Parse(scaY.text);
		scaZValueDefault = float.Parse(scaZ.text);
		sizXValueDefault = float.Parse(sizX.text);
		sizYValueDefault = float.Parse(sizY.text);
		sizZValueDefault = float.Parse(sizZ.text);
		rotaValueDefault = float.Parse(rota.text);
		deviValueDefault = float.Parse(devi.text);
		thicValueDefault = float.Parse(thic.text);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (chCoSlider.value != chCoValue)
        {
            chCoSlider.value = Mathf.Round(chCoSlider.value * 100f) / 100f;
            chCoValue = (int)chCoSlider.value;
            chCo.text = "" + chCoValue;
        }
        if (int.Parse(chCo.text) != chCoSlider.value)
        {
            chCoSlider.value = int.Parse(chCo.text);
            chCoValue = int.Parse(chCo.text);
        }
        if (scRaSlider.value != scRaValue)
        {
            scRaSlider.value = Mathf.Round(scRaSlider.value * 100f) / 100f;
            scRaValue = scRaSlider.value;
            scRa.text = "" + scRaValue;
        }
        if (float.Parse(scRa.text) != scRaSlider.value)
        {
            scRaSlider.value = float.Parse(scRa.text);
            scRaValue = float.Parse(scRa.text);
        }
        if (scaXSlider.value != scaXValue)
        {
            scaXSlider.value = Mathf.Round(scaXSlider.value * 100f) / 100f;
            scaXValue = scaXSlider.value;
            scaX.text = "" + scaXValue;
        }
        if (float.Parse(scaX.text) != scaXSlider.value)
        {
            scaXSlider.value = float.Parse(scaX.text);
            scaXValue = float.Parse(scaX.text);
        }
        if (scaYSlider.value != scaYValue)
        {
            scaYSlider.value = Mathf.Round(scaYSlider.value * 100f) / 100f;
            scaYValue = scaYSlider.value;
            scaY.text = "" + scaYValue;
        }
        if (float.Parse(scaY.text) != scaYSlider.value)
        {
            scaYSlider.value = float.Parse(scaY.text);
            scaYValue = float.Parse(scaY.text);
        }
        if (scaZSlider.value != scaZValue)
        {
            scaZSlider.value = Mathf.Round(scaZSlider.value * 100f) / 100f;
            scaZValue = scaZSlider.value;
            scaZ.text = "" + scaZValue;
        }
        if (float.Parse(scaZ.text) != scaZSlider.value)
        {
            scaZSlider.value = float.Parse(scaZ.text);
            scaZValue = float.Parse(scaZ.text);
        }
        if (sizXSlider.value != sizXValue)
        {
            sizXSlider.value = Mathf.Round(sizXSlider.value * 100f) / 100f;
            sizXValue = sizXSlider.value;
            sizX.text = "" + sizXValue;
        }
        if (float.Parse(sizX.text) != sizXSlider.value)
        {
            sizXSlider.value = float.Parse(sizX.text);
            sizXValue = float.Parse(sizX.text);
        }
        if (sizYSlider.value != sizYValue)
        {
            sizYSlider.value = Mathf.Round(sizYSlider.value * 100f) / 100f;
            sizYValue = sizYSlider.value;
            sizY.text = "" + sizYValue;
        }
        if (float.Parse(sizY.text) != sizYSlider.value)
        {
            sizYSlider.value = float.Parse(sizY.text);
            sizYValue = float.Parse(sizY.text);
        }
        if (sizZSlider.value != sizZValue)
        {
            sizZSlider.value = Mathf.Round(sizZSlider.value * 100f) / 100f;
            sizZValue = sizZSlider.value;
            sizZ.text = "" + sizZValue;
        }
        if (float.Parse(sizZ.text) != sizZSlider.value)
        {
            sizZSlider.value = float.Parse(sizZ.text);
            sizZValue = float.Parse(sizZ.text);
        }
        if (rotaSlider.value != rotaValue)
        {
            rotaSlider.value = Mathf.Round(rotaSlider.value * 100f) / 100f;
            rotaValue = rotaSlider.value;
            rota.text = "" + rotaValue;
        }
        if (float.Parse(rota.text) != rotaSlider.value)
        {
            rotaSlider.value = float.Parse(rota.text);
            rotaValue = float.Parse(rota.text);
        }
        if (deviSlider.value != deviValue)
        {
            deviSlider.value = Mathf.Round(deviSlider.value * 100f) / 100f;
            deviValue = deviSlider.value;
            devi.text = "" + deviValue;
        }
        if (float.Parse(devi.text) != deviSlider.value)
        {
            deviSlider.value = float.Parse(devi.text);
            deviValue = float.Parse(devi.text);
        }
        if (thicSlider.value != thicValue)
        {
            thicSlider.value = Mathf.Round(thicSlider.value * 100f) / 100f;
            thicValue = thicSlider.value;
            thic.text = "" + thicValue;
        }
        if (float.Parse(thic.text) != thicSlider.value)
        {
            thicSlider.value = float.Parse(thic.text);
            thicValue = float.Parse(thic.text);
        }
    }

	public void showPane() {
		transform.gameObject.SetActive (true);
		shower.enablePane = true;
	}

	public void hidePane() {
		transform.gameObject.SetActive (false);
		shower.enablePane = false;
        paneButton.SetActive(true);
    }
		
	public void defaultParameters() {
		chCoSlider.value = chCoValueDefault;
		scRaSlider.value = scRaValueDefault;
		scaXSlider.value = scaXValueDefault;
		scaYSlider.value = scaYValueDefault;
		scaZSlider.value = scaZValueDefault;
		sizXSlider.value = sizXValueDefault;
		sizYSlider.value = sizYValueDefault;
		sizZSlider.value = sizZValueDefault;
		rotaSlider.value = rotaValueDefault;
		deviSlider.value = deviValueDefault;
		thicSlider.value = thicValueDefault;
	}

}
