﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowHideChamber : MonoBehaviour {

    GameObject foramini;
    ChamberBuilder foraminifera;
    public Button bn;
    public Button bp;

    public void showChamber()
    {
            foraminifera.currentId++;
            foraminifera.chambers.ToArray()[foraminifera.currentId].transform.parent.Find("aperture").gameObject.SetActive(true);
            foraminifera.chambers.ToArray()[foraminifera.currentId].gameObject.SetActive(true);
    }

    public void hideChamber()
    {
            foraminifera.chambers.ToArray()[foraminifera.currentId].gameObject.SetActive(false);
            foraminifera.chambers.ToArray()[foraminifera.currentId].transform.parent.Find("aperture").gameObject.SetActive(false);
            foraminifera.currentId--;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (foramini == null)
        {
            bn.enabled = false;
            bn.GetComponent<Image>().color = Color.black;
            bp.enabled = false;
            bp.GetComponent<Image>().color = Color.black;
            foramini = GameObject.Find("foraminifera");
         //   Debug.Log("I'm set, foraminifera");
        }
        else
        {
            foraminifera = foramini.GetComponent<ChamberBuilder>();
            if (foraminifera != null)
            {
                if (foraminifera.currentId > -1)
                {
                    if (foraminifera.currentId == foraminifera.id && bn.enabled)
                    {
                        bn.enabled = false;
                        bn.GetComponent<Image>().color = Color.black;
                    }
                    else if (foraminifera.currentId < foraminifera.id && !bn.enabled)
                    {
                        bn.enabled = true;
                        bn.GetComponent<Image>().color = Color.white;
                    }
                    if (foraminifera.currentId == 0 && bp.enabled)
                    {
                        bp.enabled = false;
                        bp.GetComponent<Image>().color = Color.black;
                    }
                    else if (foraminifera.currentId > 0 && !bp.enabled)
                    {
                        bp.enabled = true;
                        bp.GetComponent<Image>().color = Color.white;
                    }
                }

            }
        }
	}
}
