﻿using UnityEngine;
using System.Collections;

public class Cut : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void cut() {
		GameObject go = GameObject.Find ("Foraminifera");
		for (int i = 0; i < go.transform.childCount; i++) {
			go.transform.GetChild (i).gameObject.GetComponent<Chamber> ().MeshPrepare ();
		}
	}
}
