﻿using UnityEngine;
using System.Collections;

public class Raycaster : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Ray ray = new Ray (transform.position, Vector3.down);
		if (Physics.Raycast (ray, out hit))
			Debug.DrawRay (ray.origin, ray.direction*5,Color.blue);
//			Debug.Log (hit.point + "  " + hit.distance);
	}
}
