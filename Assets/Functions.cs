﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Functions : MonoBehaviour {
	
	public enum Wheris {
		OUT, IN, BORDER
	};

	public enum Outside {
		UNO, DUO, TRE, UNODUO, UNOTRE, DUOTRE
	};

	public static Outside whoIsOutside(Vector3 uno, Vector3 duo, Vector3 tre, List<Chambre> chambers) {
		bool unol = false;
		bool duol = false;
		bool trel = false;
		int un = 0;
		int du = 0;
		int tr = 0;
		int counter = 0;
		foreach (Chambre chamber in chambers) {
			counter++;
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, uno)) {
					un++;
					break;
				}
			}
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, duo)) {
					du++;
					break;
				}
			}
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, tre)) {
					tr++;
					break;
				}
			}
		}
		if (un == counter)
			unol = true;
		if (du == counter)
			duol = true;
		if (tr == counter)
			trel = true;
		if (unol && duol)
			return Outside.UNODUO;
		if (unol && trel)
			return Outside.UNOTRE;
		if (duol && trel)
			return Outside.DUOTRE;
		if (unol)
			return Outside.UNO;
		if (duol)
			return Outside.DUO;
		return Outside.TRE;
	}
	//
	//
	//
	//

	public static bool pointOnRight(Vector3 a, Vector3 b, Vector3 c, Vector3 x) {
		Vector3 bp = b - a;
		Vector3 cp = c - a;
		Vector3 xp = x - a;

		// bX bY bZ
		// cX cY cZ
		// xX xY xZ
		float det = bp.x*cp.y*xp.z + bp.y*cp.z*xp.x + bp.z*cp.x*xp.y - bp.x*cp.z*xp.y - bp.y*cp.x*xp.z - bp.z*cp.y*xp.x;
		return det > 0;
	}

	public static bool OLDpointOutsideCurrentShape(Vector3 uno, Vector3 duo, Vector3 tre, List<Chambre> chambers) {
		foreach (Chambre chamber in chambers) {
			foreach (Triangle triangle in chamber.triangles) {				
				if (pointOnLeft (triangle.aa, triangle.bb, triangle.cc, uno))
					return false;
				if (pointOnLeft (triangle.aa, triangle.bb, triangle.cc, duo))
					return false;
				if (pointOnLeft (triangle.aa, triangle.bb, triangle.cc, tre))
					return false;
			}
		}
		return true;
	}


	public static Wheris pointPosition(Vector3 uno, Vector3 duo, Vector3 tre, List<Chambre> chambers) {
		bool unol = true;
		bool duol = true;
		bool trel = true;
		int counter = 0;
		int inter = 0;
		int middl = 0;
		int outer = 0;
	//	Debug.Log (chambers);
		foreach (Chambre chamber in chambers) {
			counter++;
			// TODO: obecnie jeśli jest na zewnątrz któregokolwiek, to jest na zewnątrz, a trzeba to odwrócić żeby było wewnątrz, jeśli jest wewnątrz któregokolwiek
			// Wewnętrzny foreach sprawdza, czy jest na zewnątrz danej komory, jeśli którykolwiek wewnętrzny foreach będzie wewnątrz, to ???

			int i = 0;
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, uno)) {
					unol = false;
					i++;
					break;
				}
			}
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, duo)) {
					duol = false;
					i++;
					break;
				}
			}
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, tre)) {
					trel = false;
					i++;
					break;
				}
			}
			if (i == 0)
				inter++;
			else if (i == 3)
				outer++;
			else
				middl++;
		}

//		int sum = un / counter + du / counter + tr / counter;
		if (inter > 0)
			return Wheris.IN;
		else if (middl > 0)
			return Wheris.BORDER;
		else
			return Wheris.OUT;
	}

	public static bool pointOnLeft(Vector3 a, Vector3 b, Vector3 c, Vector3 x) {
		Vector3 bp = b - a;
		Vector3 cp = c - a;
		Vector3 xp = x - a;

		// bX bY bZ
		// cX cY cZ
		// xX xY xZ
		float det = bp.x*cp.y*xp.z + bp.y*cp.z*xp.x + bp.z*cp.x*xp.y - bp.x*cp.z*xp.y - bp.y*cp.x*xp.z - bp.z*cp.y*xp.x;
		return det >= 0;
	}

	public static bool pointInBorder(Vector3 uno, Vector3 duo, Vector3 tre, List<Chambre> chambers) {
		bool unol = true;
		bool duol = true;
		bool trel = true;
		int i = 0;
		//Debug.Log (uno + " " + duo + " " + tre);
		foreach (Chambre chamber in chambers) {
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, uno)) {
					unol = false;
					i++;
					break;
				}
			}
			if (!unol)
				break;
		}

		foreach (Chambre chamber in chambers) {
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, duo)) {
					duol = false;
					i++;
					break;
				}
			}
			if (!duol)
				break;
		}

		foreach (Chambre chamber in chambers) {
			foreach (Triangle triangle in chamber.triangles) {
				if (pointOnRight (triangle.aa, triangle.bb, triangle.cc, tre)) {
					trel = false;
					i++;
					break;
				}
			}
			if (!trel)
				break;
		}
		//Debug.Log (i);
		if ((unol && duol && trel) || (!unol && !duol && !trel))
			return false;
		else
			return true;
	}



















	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
