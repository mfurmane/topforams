﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChamberBuilder : MonoBehaviour {

    public int currentId = -1;

	public Material[] materials;

    public int chamberCount = 1;
    public int chamberCount2 = 1;
    public float growth_vector_scaling_rate = 1;
	public Vector3 chamber_scaling_rates = Vector3.one;
	public Vector3 first_chamber_size = Vector3.one;
	public float rotation_angle = 0;
	public float deviation_angle = 0;	//odchylenie
    public float thickness = 0;

	public Material material;
    public List<Chambre> chambers;
    public List<Chambre> interiors = new List<Chambre>();

    private Vector3 curO;
	private Transform curU;
    private Transform preU;
    private Transform prepreU;
    private Vector3 growthAxis = Vector3.zero;
	private float maxLength;
	private Vector3 nextVector;
	//private Vector3 prevVector;
	private Vector3 nextCenter;
    private bool start = false;
    private bool interiorCleaned = false;
    private bool chamberEnded = false;
    public int id = 0;
    private int lastId = -1;
    private Vector3 size;
    private Chambre cha;
    SphereCollider prevapert;
    public bool cutEnded = false;
    public Bounds foraBounds;

    public Simulate simlt;

    public List<ToFill> toFillAfterCut = new List<ToFill>();

    public void cut(Transform v1, Transform v2, Transform v3, Transform v4, Transform transform)
    {
        Vector3 one = v2.position - v1.position;
        Vector3 two = v3.position - v1.position;
        Debug.Log("CHAMBERS LENGTH: "+chambers.ToArray().Length);
        foreach (Chambre c in chambers)
        {
            List<Triangle> newTriangs = new List<Triangle>();
            //Debug.Log("TRIANGLE: "+c.triangles.ToArray()[0].transform.TransformPoint(c.triangles.ToArray()[0].aa) +" - "+ c.transform.Find(c.name+"(Clone)").GetComponent<Chambre>().triangles.ToArray()[0].transform.TransformPoint(c.triangles.ToArray()[0].aa));
            foreach (Triangle triang in c.triangles)
            {
                //if (triang.gameObject.activeSelf)
              //  {
                    Vector3 threea = triang.transform.TransformPoint(triang.aa) - v1.position;
                    Vector3 threeb = triang.transform.TransformPoint(triang.bb) - v1.position;
                    Vector3 threec = triang.transform.TransformPoint(triang.cc) - v1.position;
                    float deta = one.x * two.y * threea.z + two.x * threea.y * one.z + threea.x * one.y * two.z - one.z * two.y * threea.x - one.y * two.x * threea.z - one.x * two.z * threea.y;
                    float detb = one.x * two.y * threeb.z + two.x * threeb.y * one.z + threeb.x * one.y * two.z - one.z * two.y * threeb.x - one.y * two.x * threeb.z - one.x * two.z * threeb.y;
                    float detc = one.x * two.y * threec.z + two.x * threec.y * one.z + threec.x * one.y * two.z - one.z * two.y * threec.x - one.y * two.x * threec.z - one.x * two.z * threec.y;
                    int i = 0;
                    if (deta < 0) i++; if (detb < 0) i++; if (detc < 0) i++;

                    if (i > 0)
                    {
                        triang.gameObject.SetActive(false);
                        if (i < 3)
                        {
                            List<Triangle> trls = new ToFill(deta < 0, detb < 0, detc < 0, triang.transform.TransformPoint(triang.aa), triang.transform.TransformPoint(triang.bb), triang.transform.TransformPoint(triang.cc), c.transform.position, true).createTriangles();
                            foreach (Triangle trl in trls)
                            {
                                trl.transform.SetParent(c.transform);
                                trl.gameObject.GetComponent<MeshRenderer>().material = material;
                                newTriangs.Add(trl);
                            }

                            //                        toFillAfterCut.Add(new ToFill(deta < 0, detb < 0, detc < 0, triang.transform.TransformPoint(triang.aa), triang.transform.TransformPoint(triang.bb), triang.transform.TransformPoint(triang.cc), c.transform.position));
                        }
                    } else triang.gameObject.SetActive(true);
              //  }
            }
            foreach (Triangle triang in newTriangs)
            {
                c.triangles.Add(triang);
            }
            if (c.transform.parent.Find("aperture") != null)
            {
                Vector3 ap = c.transform.parent.Find("aperture").position;
                Vector3 three = ap - v1.position;
                float det = one.x * two.y * three.z + two.x * three.y * one.z + three.x * one.y * two.z - one.z * two.y * three.x - one.y * two.x * three.z - one.x * two.z * three.y;
                if (det < 0)
                    c.transform.parent.Find("aperture").gameObject.SetActive(false);
            }
        }


        Debug.Log(v1.position+" - "+ v2.position + " - " + v3.position + " - " + v4.position);
        cutEnded = true;
    }

    void objectPreparation(Vector3 radius, Vector3 position, int id)
    {
        //OBJECTS MANAGEMENT
        GameObject chambreKeeper = new GameObject();
        chambreKeeper.transform.SetParent(transform);
        chambreKeeper.name = "chambre keeper";
        GameObject chamber = new GameObject();
        cha = chamber.AddComponent<Chambre>();
        cha.chambers = chambers;
        cha.transform.SetParent(chambreKeeper.transform);

        //SHAPE GENERATION
        cha.buildChamber(radius, position, id);
        cha.material = material;

        //ROTATION OF CHAMBER - curU is really preU here
        if (id > 0)
        {
            cha.transform.parent.up = position - curU.position;
            cha.cleanInterior();
        }
        else cha.cleaningFinished = true;
    }

    Chambre nextGrowth(Vector3 radius, Vector3 position, int id) {

        //INTERIOR
        if (ModeManager.mode == ModeManager.Mode.CHAMBERS_VIEWING)
        {
            GameObject interior = Instantiate(cha.gameObject);
            interior.transform.position = cha.transform.position;
            interior.transform.rotation = cha.transform.rotation;
            interior.transform.parent = cha.transform;
            interior.transform.localScale = new Vector3(thickness, thickness, thickness);
        }


        // curU.parent = chambreKeeper.transform;

        if (id < chamberCount - 1)
        {
            growthAxis = (curU.position - preU.position).normalized;
            //DrawAxis(preU.position, curU.position);

            maxLength = cha.radius.y;
            float length = maxLength * growth_vector_scaling_rate;
            nextVector = growthAxis * length;
            Quaternion deviation;
            Quaternion rotation;

            //TODO deviate by deviation angle
            if (id == 0)
            {
                deviation = Quaternion.Euler(deviation_angle, 0, 0);
            }
            else
            {
                Plane plane = new Plane();
                plane.Set3Points(prepreU.position, preU.position, curU.position);
                deviation = Quaternion.AngleAxis(deviation_angle, plane.normal);
            }
            //TODO rotate by rotation angle
            rotation = Quaternion.AngleAxis(rotation_angle, growthAxis);

            nextVector = rotation * deviation * nextVector;

            //Debug.Log(cha.transform.parent.Find("aperture").position + " - "+ nextVector);

            nextCenter = curU.position + nextVector;
            //DrawVector(curU.position, nextCenter);
        }

        return cha;
	}

	public void simulate() {
		transform.name = "foraminifera";
		chambers = new List<Chambre> ();
		Debug.Log ("SZTART");
		size = first_chamber_size;
		curU = aperturePosition(Vector3.zero);
		nextCenter = Vector3.zero;
        start = true;
	}
		

	// Use this for initialization
	void Start () {

    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (chamberEnded && id < chamberCount-1)
        {
            id += 1;
            size = new Vector3(size.x * chamber_scaling_rates.x, size.y * chamber_scaling_rates.y, size.z * chamber_scaling_rates.z);

        /*    GameObject go = Instantiate(cha, cha.transform).gameObject;
            go.transform.position = cha.transform.position;
            go.transform.rotation = cha.transform.rotation;
            go.transform.localScale = cha.transform.localScale * 0.9f;
            */
            chamberEnded = false;
            start = true;
        }

        if (chamberEnded && id == chamberCount - 1)
        {
            if (ModeManager.mode == ModeManager.Mode.CHAMBERS_VIEWING)
                for (int i = 0; i < chamberCount; i++)
                {
                    chambers.Add(chambers.ToArray()[i].transform.Find(chambers.ToArray()[i].name + "(Clone)").GetComponent<Chambre>());
                }
            gameObject.AddComponent<Rotatable>();
            if (simlt != null )simlt.enableTools();
            Debug.Log("Wszystkie dodane, liczba komór: " + chambers.ToArray().Length);
            chamberCount2 = chamberCount;

            foraBounds = new Bounds(transform.position, Vector3.one * 0.1f);

            for (int j = 0; j < chambers.ToArray().Length / 2; j++)
            {
                Chambre cha = chambers.ToArray()[j];
                Renderer[] renderers = cha.gameObject.GetComponentsInChildren<Renderer>();
                foreach (Renderer renderer in renderers)
                {
                    foraBounds.Encapsulate(renderer.bounds);
                }
            }

            chamberCount = 0;
            currentId = id;
            simlt.progress.SetActive(false);
        }

            if (interiorCleaned)
        {
            //APERTURE FINDING
            //    if (id > 0) prevapert.enabled = false;
            cha.findAperture();
      //      if (id > 0) prevapert.enabled = true;
            chambers.Add(cha);
            //APERTURES MANAGEMENT
            prepreU = preU;
            preU = curU;
            curU = aperturePosition(cha.transform.parent.Find("aperture").position);
          //  prevapert = cha.transform.parent.Find("aperture").GetComponent<SphereCollider>();

            nextGrowth(size, nextCenter, id);
            chamberEnded = true;

            interiorCleaned = false;
        }
        if (cha !=null) if (cha.cleaningFinished) {
            interiorCleaned = true;
                cha.cleaningFinished = false;
        }
        //transform.LookAt (Input.mousePosition);
        if (start)
        {
            objectPreparation(size, nextCenter, id);

            start = false;
        }

    }

    private Transform aperturePosition(Vector3 position)
    {
        GameObject aper = new GameObject();
        aper.transform.parent = GameObject.Find("Main Camera").transform;
        aper.transform.position = position;
        aper.name = "apperitiff";
        return aper.transform;
    }



    public void DrawAxis(Vector3 start, Vector3 end)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.transform.parent = transform;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        //lr.SetColors(color, color);
        //        lr.SetWidth(0.1f, 0.1f);
        lr.startColor = Color.blue;
        lr.endColor = Color.red;
        lr.startWidth = 0.01f;
        lr.endWidth = 0.01f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

    public void DrawVector(Vector3 start, Vector3 end)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.transform.parent = transform;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        //lr.SetColors(color, color);
        //        lr.SetWidth(0.1f, 0.1f);
        lr.startColor = Color.black;
        lr.endColor = Color.yellow;
        lr.startWidth = 0.01f;
        lr.endWidth = 0.01f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }



}
