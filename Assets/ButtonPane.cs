﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ButtonPane : MonoBehaviour {

    Button cutB;
    public Button paneB;
    Image cutI;

	// Use this for initialization
	void Awake ()
    {
        if (transform.Find("Cut") != null)
        {
            cutB = transform.Find("Cut").GetComponent<Button>();
            cutI = transform.Find("Cut").GetComponent<Image>();
        }
        gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void turnDownButtonPane()
    {
        gameObject.SetActive(false);
    }
    public void turnUpButtonPane()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
        }
        else
        {
            gameObject.SetActive(true);
        }
        if (cutI != null)
        {
            if (GameObject.Find("foraminifera") == null)
            {
                cutI.color = Color.black;
                cutB.enabled = false;
            }
            else
            {
                cutI.color = Color.white;
                cutB.enabled = true;
            }
        }
    }

}
