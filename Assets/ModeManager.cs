﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModeManager : MonoBehaviour {

    public enum Mode
    {
        MUTATION_COLONY,
        CHAMBERS_VIEWING
    };

    public Button add;
    public Button remov;
    public Button next;
    public Button prev;
    public Sprite sn;
    public Sprite sp;
    public CuttingPlane cutPlane;
    public static Mode mode = Mode.CHAMBERS_VIEWING;
    public MutationPopup mutPop;
    public MutationManager mutMan;
    public GameObject rotationWindow;
    public Button showNormal;
    public Button showMutas;
    private Button showActual;

    public void hideButton()
    {
        if (mode == Mode.MUTATION_COLONY)
            rotationWindow.SetActive(false);
        showActual.gameObject.SetActive(false);
    }
    public void showButton()
    {
        if (mode == Mode.MUTATION_COLONY)
            rotationWindow.SetActive(true);
        showActual.gameObject.SetActive(true);
    }

    ChamberBuilder foraminifera;
    public Solidoparent sopa;

    ChamberBuilder generate()
    {
		GetComponent<MutationManager> ().progress.SetActive (true);//mutMan.progress.SetActive (true);
        sopa.setSolid();
        GameObject newBuilder = new GameObject();
        ChamberBuilder builder = newBuilder.AddComponent<ChamberBuilder>();
        // newBuilder.AddComponent<Rotatable>();
        builder.material = foraminifera.material;
        builder.materials = foraminifera.materials;

        builder.chamberCount = foraminifera.chamberCount2;
        builder.growth_vector_scaling_rate = foraminifera.growth_vector_scaling_rate;
        builder.chamber_scaling_rates = foraminifera.chamber_scaling_rates;
        builder.first_chamber_size = foraminifera.first_chamber_size;
        builder.rotation_angle = foraminifera.rotation_angle;
        builder.deviation_angle = foraminifera.deviation_angle;
        builder.thickness = foraminifera.thickness;
        builder.simlt = foraminifera.simlt;

        builder.simulate();
        Destroy(foraminifera.gameObject);
        return builder;
    }

    public void switch_mode()
    {
        /*  Sprite ssn = next.GetComponent<Image>().sprite;
          Sprite ssp = prev.GetComponent<Image>().sprite;
          next.GetComponent<Image>().sprite = sn;
          prev.GetComponent<Image>().sprite = sp;
          sn = ssn;
          sp = ssp;*/

        add.gameObject.SetActive(!add.gameObject.activeSelf);
        remov.gameObject.SetActive(!remov.gameObject.activeSelf);
        next.gameObject.SetActive(!next.gameObject.activeSelf);
        prev.gameObject.SetActive(!prev.gameObject.activeSelf);

        if (mode == Mode.CHAMBERS_VIEWING)
        {
            mode = Mode.MUTATION_COLONY;
          //  cutPlane.transform.Rotate(new Vector3(90, 0, 0));
            rotationWindow.SetActive(true);
            showActual = showMutas;
        }
        else
        {
            mode = Mode.CHAMBERS_VIEWING;
          //  cutPlane.transform.Rotate(new Vector3(-90, 0, 0));
            rotationWindow.SetActive(false);
            showActual = showNormal;
        }
        GameObject tmpo = GameObject.Find("foraminifera");
        Debug.Log(tmpo);
        if (tmpo != null)
        {
            mutPop.gameObject.SetActive(true);
            foraminifera = tmpo.GetComponent<ChamberBuilder>();
            mutPop.setValues(foraminifera.chamberCount2, foraminifera.growth_vector_scaling_rate, foraminifera.chamber_scaling_rates, foraminifera.first_chamber_size, foraminifera.deviation_angle, foraminifera.rotation_angle, foraminifera.thickness, foraminifera.foraBounds, foraminifera.material, foraminifera.materials);
            generate();
            mutPop.gameObject.SetActive(false);
        }

    }

    // Use this for initialization
    void Start () {
        deb = debugg.transform.Find("Text").GetComponent<Text>();
        showActual = showNormal;
	}

    public GameObject debugg;
    public static Text deb;
    private float tapTime = 0;
	// Update is called once per frame
	void Update () {
        {
          /*  Debug.Log(tapTime);
            if (Input.GetMouseButton(0))
            {
                tapTime += Time.deltaTime;
                if (tapTime > 1)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider.transform.parent != null)
                        {
                            if (hit.collider.transform.parent.name == "filling triangles keeper")
                            {
                                if (hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>() != null)
                                {
                                    ChamberBuilder mother = hit.collider.transform.parent.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>();
                                    mutPop.gameObject.SetActive(true);
                                    mutPop.setValues(mother.chamberCount2, mother.growth_vector_scaling_rate, mother.chamber_scaling_rates, mother.first_chamber_size, mother.deviation_angle, mother.rotation_angle, mother.thickness, mother.foraBounds, mother.material, mother.materials);
                                }
                            }
                            else if (hit.collider.transform.parent.parent != null)
                            {
                                if (hit.collider.transform.parent.parent.name == "chambre keeper")
                                    if (hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>() != null)
                                    {
                                        ChamberBuilder mother = hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>();
                                        mutPop.gameObject.SetActive(true);
                                        mutPop.setValues(mother.chamberCount2, mother.growth_vector_scaling_rate, mother.chamber_scaling_rates, mother.first_chamber_size, mother.deviation_angle, mother.rotation_angle, mother.thickness, mother.foraBounds, mother.material, mother.materials);
                                    }
                            }
                        }
                    }
                    //                        mutPop.setValues();
                    tapTime = 0;
                }
            }
            else tapTime = 0;*/

            
            if (Input.touches.Length == 1)
            {
                Touch touch = Input.touches[0];
                if (touch.tapCount == 1)
                {
                    tapTime += Time.deltaTime;
                    if (tapTime > 1)
                    {
                        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.transform.parent != null)
                            {
                                if (hit.collider.transform.parent.name == "filling triangles keeper")
                                {
                                    if (hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>() != null)
                                    {
                                        ChamberBuilder mother = hit.collider.transform.parent.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>();
                                        mutPop.gameObject.SetActive(true);
                                        mutPop.setValues(mother.chamberCount2, mother.growth_vector_scaling_rate, mother.chamber_scaling_rates, mother.first_chamber_size, mother.deviation_angle, mother.rotation_angle, mother.thickness, mother.foraBounds, mother.material, mother.materials);
                                    }
                                }
                                else if (hit.collider.transform.parent.parent != null)
                                {
                                    if (hit.collider.transform.parent.parent.name == "chambre keeper")
                                        if (hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>() != null)
                                        {
                                            ChamberBuilder mother = hit.collider.transform.parent.parent.parent.gameObject.GetComponent<ChamberBuilder>();
                                            mutPop.gameObject.SetActive(true);
                                            mutPop.setValues(mother.chamberCount2, mother.growth_vector_scaling_rate, mother.chamber_scaling_rates, mother.first_chamber_size, mother.deviation_angle, mother.rotation_angle, mother.thickness, mother.foraBounds, mother.material, mother.materials);
                                        }
                                }
                            }
                        }
                        //                        mutPop.setValues();
                        tapTime = 0;
                    }
                }
            } else tapTime = 0;
            
        }

    }
}
